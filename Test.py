import pandas as pd


df_open = pd.read_csv('./Input/US_MS/Open.csv', parse_dates = True, index_col=0)
df_open = df_open.reset_index().melt(id_vars='Date', var_name="Ticker", value_name='Open')

df_high = pd.read_csv('./Input/US_MS/High.csv', parse_dates = True, index_col=0)
df_high = df_high.reset_index().melt(id_vars='Date', var_name="Ticker", value_name='High')

df_low = pd.read_csv('./Input/US_MS/Low.csv', parse_dates = True, index_col=0)
df_low = df_low.reset_index().melt(id_vars='Date', var_name="Ticker", value_name='Low')

df_close = pd.read_csv('./Input/US_MS/Close.csv', parse_dates = True, index_col=0)
df_close = df_close.reset_index().melt(id_vars='Date', var_name="Ticker", value_name='Close')

df_volume = pd.read_csv('./Input/US_MS/Volume1.csv', parse_dates = True, index_col=1)
df_volume = df_volume.reset_index().melt(id_vars='Date', var_name="Ticker", value_name='Volume')


df_data = pd.merge(df_open, df_high, left_on=['Date', 'Ticker'], right_on=['Date', 'Ticker'], how='inner')
df_data = pd.merge(df_data, df_low, left_on=['Date', 'Ticker'], right_on=['Date', 'Ticker'], how='inner')
df_data = pd.merge(df_data, df_close, left_on=['Date', 'Ticker'], right_on=['Date', 'Ticker'], how='inner')
df_data = pd.merge(df_data, df_volume, left_on=['Date', 'Ticker'], right_on=['Date', 'Ticker'], how='inner')


df_data['Date'] = df_data['Date'].dt.strftime("%Y%m%d")
df_data.sort_values(['Date','Ticker']).to_csv('./Input/US_MS/US_MS.csv', encoding='euc-kr', index=False)


