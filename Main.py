from SAA import *
from TAA import *
from OPT import *
#from KILL import *
from SIMUL import *
from BackTest import *
from MP import *
from datetime import datetime

if __name__ == "__main__":

    # input parameter
    saa_method = "Hybrid"  # HMM / ML / Hybrid
    taa_method = "Hybrid"  # NH / DM / ML / Hybrid
    opt_method = "EW"   # RB / HRP / EW

    company_code = 'FANH01'
    product_code = 'K01'

    start_date = "2016-01-01"
    end_date = "2019-11-30"

    country = 'US' if product_code == 'K01' else 'KR'


    # 00. 유니버스 및 데이터 로드
    df_data = get_time_series_data(country, "ETF")
    df_universe = get_universe_data(company_code, product_code)
    # 유니버스에 시세 데이터가 없는 종목 제거
    df_universe = pd.merge(pd.DataFrame(df_data.reset_index()['Symbol'].unique(), columns=['Ticker']), df_universe,
                           left_on='Ticker', right_on='Ticker', how='inner')

    # 테스트 기간 영업일 정보
    date_list, date_list_all = get_operation_date(df_data, start_date, end_date)
    # 테스트 기간 리밸런싱 일자 정보
    interval_list = make_interval_date(date_list, date_list[0], date_list[-1], 1)
    baseMPday_list = make_baseMP_date(date_list_all, interval_list, 1)
    # MP 생성시 전 영업일 기준으로 하기때문에 start_date를 1일 강제로 전 영업일로 세팅 변경함.
    start_date = date_list_all[date_list_all.index(date_list[0]) - 1].strftime("%Y-%m-%d")
    # 입력한 마지막 일자를 데이터가 있는 일자중에 마지막 일자로 변경(프로그램 실행시 오류 막기 위함)
    # product에서는 당일 or 최근 영업일로 세팅
    end_date = date_list_all[date_list_all.index(date_list[-1])].strftime("%Y-%m-%d")

    # 01. SAA 국면/비중 산출
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- SAA_start")
    df_saa_ratio = SAA_main(company_code, product_code, saa_method, start_date, end_date, rebalancing_period=20, convert=False, plot=False, economic=False)

    # 02. TAA 종목 선택
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- TAA_start")
    df_selected_score = TAA_main(company_code, product_code, taa_method, start_date, end_date, df_data, df_universe)
    df_selected_score.index.names = ['index']
    df_taa_list = df_selected_score.reset_index().melt(id_vars='index', var_name="Ticker", value_name='Score')
    df_taa_list = df_taa_list[df_taa_list['Score'] > 1e-6]
    df_taa_list = pd.merge(df_taa_list, df_universe, left_on='Ticker', right_on='Ticker', how='inner').sort_values(['index', 'Large','Mid'])
    df_taa_list = df_taa_list.rename(columns={'index': 'Date'}).set_index(['Date','Ticker'])
    df_taa_list.to_csv('df_taa_list.csv')


    # start_date, end_date 테스트 기준일로 재산정 필요 signurs

    # 03. KillSwitch 개별 테스트
    # input : Ticker, 시계열 자료, 종목코드
    # output : kill switch가 작동한 종목 리스트
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- KILL_start")
    #df_killed = Kill_main(df_data,universe,df_universe)
    #df_killed=pd.read_csv(universe+'/Intermediate/df_killed.csv', parse_dates = True, index_col=0)

    # 04. Optimization 비중 산출
    # input : SAA 비중, Selection 유니버스, 유니버스 시계열, Opt method
    # output : 포트폴리오 비중[일자, 종목, 비중]

    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- OPT_start")
    df_weight = OPT_NH(df_saa_ratio, df_taa_list, df_data, opt_method, start_date, end_date, df_universe, baseMPday_list)
    #df_weight, rebal = OPT_KO(df_saa_ratio, df_selected_score, df_data, opt_method, "2019-01-01", "2019-08-30", df_universe, universe, df_killed)

    df_weight_list = df_weight.reset_index().melt(id_vars='index', var_name="Ticker", value_name='Ratio')
    df_weight_list = df_weight_list[df_weight_list['Ratio'] > 1e-6]
    df_weight_list = pd.merge(df_weight_list, df_universe, left_on='Ticker', right_on='Ticker', how='inner').sort_values(['index', 'Large','Mid'])
    df_weight_list = df_weight_list.rename(columns={'index': 'Date'}).sort_values(['Date','Large']).set_index(['Date','Ticker'])
    df_weight_list.to_csv("./" + company_code + "/" + product_code + '/Intermediate/taa_list.csv')

    # 05. 시뮬레이션
    # input : 포트폴리오 비중, kill list
    # output : 수익률 그래프, MDD 그래프
    '''
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- BackTest_start")
    df_pv = Backtesting(df_saa_ratio,df_data, df_weight,start_date,end_date,df_universe,rebalancing_period=20)
    df_pv.to_csv(company_code + '/' + product_code+'/Intermediate/df_pv.csv')
    pv_value = multi_plot(df_pv, taa_method, saa_method,opt_method, company_code + '/' + product_code, start_date,end_date,company_code,product_code, mdd=False, bm=True, SAA_plot=False)
    mdd_value = multi_plot(df_pv, taa_method, saa_method,opt_method, company_code + '/' + product_code,start_date,end_date,company_code,product_code, mdd=True, bm=True, SAA_plot=False)
    '''


    #06. MP 생성(NH용)
    df_mp_all = make_MP(df_weight, baseMPday_list, interval_list, company_code, product_code, country)
    df_mp_all.to_csv(
        "./" + company_code + "/" + product_code + '/MP/result' + saa_method + '_' + taa_method + '_' + opt_method + '.csv',
        encoding='euc-kr', index=False)


    #07. NH 백테스트 시행
    capital = 100000
    tendency_code = '05'

    b1 = BackTest(df_mp_all, df_data, date_list, capital, 0, 0, 1, company_code, product_code, tendency_code)
    b1.run()
    b1.draw_ret_chart(product_code)

