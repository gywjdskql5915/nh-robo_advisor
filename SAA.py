#!/usr/bin/env python
# coding: utf-8
from SAA_make_model import *
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime
from static_function import *


def SAA_make_business_cycle(raw_data, economic_data, method_type, start_day, rebalancing_period, universe, convert, economic=True):
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- SAA_make_business_cycle: " + method_type)
    '''
    SAA_make_business_cycle(raw_data, method_type, start_day, rebalancing_period, universe, convert, economic=True)
    SAA_make_model.py를 활용하여 생성된 국면을 반환합니다. Universe에 의해 설정된 경로에 생성하려는 파일이 존재하면, 해당 파일을 불러옵니다.

    -------
    Parameters

    raw_data : DataFrame
        asset에 대해서 선정된 대표index의 price
    method_type : str
        Asset allocation에 사용할 모델명 입니다.
    start_day : str
        backtest에서 시작되는 날짜 입니다.
    rebalancing_period : int
        Portfolio에서 사용되는 rebalancing 주기입니다.
    universe : str
    사용하는 universe의 이름입니다. 경로를 설정하기 위해 사용됩니다.
    convert : Boolean
        convert사용여부를 결정합니다.
    economic : Boolean
        경제지표 데이터 활용여부를 결정합니다.

    -------
    Returns
    state : DataFrame
        SAA_make_model.py를 활용하여 생성된 국면을 반환합니다. universe에 의해 설정된 경로에 state에 대한 value를 저장합니다.

    '''

    if economic == True and universe+'/Intermediate\\{}_economy_state.csv'.format(method_type) in glob.glob(universe+'/Intermediate/*.csv'):
        print('SAA_{}_economy_state.csv exist'.format(method_type))
        state = pd.read_csv(universe+'/Intermediate/{}_economy_state.csv'.format(method_type), parse_dates = True, index_col = 0)
        state=state.apply(lambda x:x.replace(',',''))
    elif economic == False and universe+'/Intermediate\\{}_state.csv'.format(method_type) in glob.glob(universe+'/Intermediate/*.csv'):
        print('SAA_{}_state.csv exist'.format(method_type))
        state = pd.read_csv(universe+'/Intermediate/{}_state.csv'.format(method_type), parse_dates = True, index_col = 0)
        state=state.apply(lambda x:x.replace(',',''))

    else:
        if economic:
            print('make {}_economic_state'.format(method_type))
            state = SAA_make_model(method_type,start_day,rebalancing_period,raw_data,economic_data,universe,economic,convert)
            state.columns = ['State_Name']
            state.to_csv(universe+'/Intermediate/{}_economy_state.csv'.format(method_type),header=True, index=True)
        else:
            print('make {}_state'.format(method_type))
            state = SAA_make_model(method_type,start_day,rebalancing_period,raw_data,economic_data,universe,economic,convert)
            state.columns = ['State_Name']
            state.to_csv(universe+'/Intermediate/{}_state.csv'.format(method_type),header=True, index=True)
    return state


# def SAA_chart_business_cycle(df_saa_ratio, df_saa_data,start_day,universe,method_type):
#     scatter_data = df_saa_ratio[df_saa_ratio.columns[0]]
#     plot_data1 = df_saa_data.loc[start_day:][df_saa_data.columns[0]]
#     plot_data2 = df_saa_data.loc[start_day:][df_saa_data.columns[1]]
#
#     colordict_state_cycle ={"Stagflation":{'color':'orange', 'alpha': 0.2, 'ylim':[0,1]},
#                             "Inflation":{'color':'blue', 'alpha': 0.15, 'ylim':[0,1]},
#                             "Goldirocks":{'color':'yellow', 'alpha': 0.2, 'ylim':[0,1]},
#                             "Recession":{'color':'dimgray', 'alpha': 0.3, 'ylim':[0,1]}}
#
#
#     state = 0
#     val = []
#     range_dict = {'Stagflation':[],'Recession':[],'Goldirocks':[],'Inflation':[]}
#
#     for i in range(len(scatter_data)):
#         if scatter_data.iloc[i] != state:
#             if i == 0:
#                 val.append(scatter_data.iloc[i:i+1].index[0])
#             else:
#                 val.append(scatter_data.iloc[i-1:i].index[0])
#
#                 if len(val) == 2:
#                     range_dict[scatter_data.iloc[i-1]].append(val)
#                     val = []
#                 val.append(scatter_data.iloc[i:i+1].index[0])
#             state = scatter_data.iloc[i]
#     val.append(scatter_data.iloc[i:i+1].index[0])
#     range_dict[scatter_data.iloc[i]].append(val)
#
#     fig, ax = plt.subplots(figsize = [14,5])
#     ax2 = ax.twinx()
#     plot_data1.plot(ax = ax, color = 'black',linestyle='-',linewidth = 3)
#     plot_data2.plot(ax = ax2, color= 'green', linestyle='--', linewidth = 3)
#
#     for state in colordict_state_cycle.keys():
#         for i,j in range_dict[state]:
#             plt.axvspan(i,j, color=colordict_state_cycle[state]['color'], alpha=colordict_state_cycle[state]['alpha'],
#                        ymin = colordict_state_cycle[state]['ylim'][0], ymax = colordict_state_cycle[state]['ylim'][1])
#
#     import matplotlib.patches as mpatches
#     state_sort = np.sort(list(colordict_state_cycle.keys()))
#     state_legend1 = [mpatches.Patch(color=i, label=j) for i,j in zip(['black','green'],[plot_data1.name,plot_data2.name])]
#     state_legend2 = [mpatches.Patch(color=colordict_state_cycle[eachkey]['color'], label=str(eachkey)) for eachkey in state_sort]
#     plt.legend(handles=state_legend1+state_legend2)
#     plt.savefig(universe+'/Intermediate/SAA_'+method_type+'.png')


def plot_state(state, ax2, color_alpha_dict) :
        '''
        Parameter
        -----------
        state : pandas.Series (index가 일자)
        ax2 : matplotlib.axes
        color_alpha_dict : python dictionary. 각 State의 색상, 투명도 등 파라미터 인자.
                            예시 {key값 : {'color':__, 'alpha:__', 'ylim:__'}}
                            color : 색상명, alpha : 색상투명도, ylim : 색칠할 y-axis 길이 (최소0, 최대1)
        '''
        for each_state in state.unique() :
            if each_state not in color_alpha_dict :
                continue

            state_temp = state == each_state

            range_list = []
            prev_val = False
            for inx, val in state_temp.iteritems():
                #print(str(inx), str(val),str(prev_val))

                if prev_val != val:
                    if val:
                        start = inx
                    else:
                        range_list.append((start, inx))

                if (inx == state_temp.index[-1]) & val :
                    range_list.append((start, inx))

                prev_inx = inx
                prev_val = val

            # color + alpha define
            state_color = color_alpha_dict[each_state]['color']
            alpha = color_alpha_dict[each_state]['alpha']
            ymin = color_alpha_dict[each_state]['ylim'][0]
            ymax = color_alpha_dict[each_state]['ylim'][-1]

            i_cnt = 0

            for (start,end) in range_list :
                ax2.axvspan(start, end, ymin = ymin, ymax = ymax, color=state_color, alpha=alpha,
                            label = '_'*i_cnt)
                i_cnt += 1
            ax2.legend()

def SAA_chart_business_cycle(df_saa_state, df_saa_data):
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- SAA_make_business_cycle")

    # example data
    # df = pd.read_csv('Output/SAA_NH_Result.csv', index_col=0)
    # df.index = pd.to_datetime(df.index)

    colordict_state_cycle ={"Stagflation":{'color':'blue', 'alpha': .1, 'ylim':[0,1]}, # Stagflation
                            "Inflation":{'color':'pink', 'alpha': .4, 'ylim':[0,1]},  # Inflation
                            "Goldirocks":{'color':'yellow', 'alpha': .1, 'ylim':[0,1]},# Goldirocks
                            "Recession":{'color':'gray', 'alpha': .4, 'ylim':[0,1]}} # Recession

    import matplotlib.patches as mpatches
    state_sort = np.sort(list(colordict_state_cycle.keys()))
    state_legend = [mpatches.Patch(color=colordict_state_cycle[eachkey]['color'], label=str(eachkey)) for eachkey in state_sort]


    fig, ax = plt.subplots(figsize = [14,5])
    ax2 = ax.twinx()

    y_left_colname = df_saa_data.columns[0]
    y_right_colname = df_saa_data.columns[-1]
    df_saa_data[y_left_colname].plot(ax = ax, color = 'black', alpha=.8)
    df_saa_data[y_right_colname].plot(ax = ax2, color= 'green', alpha=.8, linestyle='--')
    ax.set_ylabel(y_left_colname)
    ax2.set_ylabel(y_right_colname)
    ax.legend([y_left_colname],loc = 'upper left')
    ax2.legend([y_right_colname],loc = 'upper right')

    plot_state(ax2=ax, state=df_saa_state['State_Name'], color_alpha_dict=colordict_state_cycle)
    plt.legend(handles=state_legend, loc='lower left')
    plt.show()


def SAA_get_allocation_ratio(df_saa_standard_ratio,state,method_type,universe,economic=True):
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- SAA_get_allocation_ratio")
    '''
    SAA_get_allocation_ratio(method_type,universe,economic=True)
    SAA_make_business_cycle에 의해 생성된 state를 설정한 국면의 이름으로 바꾸고, asset에 대해 ratio를 부여합니다. /Input 에 존재하는 SAA_Standard_ratio.csv에 의해 ratio가 결정되며, asset이름이 부여됩니다.

    -------
    Parameters

    method_type : str
        Asset allocation에 사용할 모델명 입니다.
    universe : str
    사용하는 universe의 이름입니다. 경로를 설정하기 위해 사용됩니다.
    economic : Boolean
        경제지표 데이터 활용여부를 결정합니다.

    -------
    Returns

    result : DataFrame
        국면과 asset에 대한 ratio가 부여된 DataFrame을 반환합니다. universe에 의해 설정된 경로에 저장합니다.

    '''

    df_saa_standard_ratio = df_saa_standard_ratio.set_index('Asset').T.reset_index()
    df_saa_standard_ratio.rename(columns={'index': 'State_Name'}, inplace=True)
    state =state.replace(2,'Inflation').replace(1,'Goldirocks').replace(-1,'Recession').replace(-2,'Stagflation')
    state.columns = ['State_Name']
    result = state.merge(df_saa_standard_ratio, on = 'State_Name',  how='left')
    result.index = state.index

    if economic:
        result.to_csv(universe+'/Intermediate/{}_economy_ratio.csv'.format(method_type),header=True, index=True)
    else:
        result.to_csv(universe+'/Intermediate/{}_ratio.csv'.format(method_type),header=True, index=True)
    return result


def SAA_chart_allocation_ratio(df_saa_ratio):
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- SAA_chart_allocation_ratio")
    '''
    SAA_chart_allocation_ratio(df_saa_ratio)
    SAA_get_allocation_ratio에 의해 생성된 데이터를 이용하여, asset에 대한 ratio가 stacked된 형태의 plot을 반환합니다.

    -------
    Parameters

    df_saa_ratio : SAA_get_allocation_ratio에 의해 생성된 DataFrame입니다.

    -------
    Returns
        Plot이 그려지고 return은 없습니다.
    '''

    df_saa_ratio.plot(stacked=True, kind='area')



def SAA_Chart_CycleLevel(kuniv_result, df_saa_data, startDay, endDay, cols_dict = {'col_growth' : 'SNP500', 'col_rates' : 'DGS10'},
                         convert = False) :
    '''
    Parameters
    -------
    kuniv_result : pd.DataFrame. SAA 국면 정의 결과 데이터.
            기본 Shape : DateIndex * [State_Name, Asset1 ... AssetN]
            컬럼 설명 :
                - State_Name : object. Goldirocks, Inflation, Recession, Stagflation의 4 국면
                - Asset1 ... AssetN : 각 Asset에 투자 ratio

    df_saa_data : pd.DataFrame.
        국면 정의에 활용되는 경기 및 물가 시계열
        기본 Shape : DateIndex * [지표1, 지표2]

    cols_dict : dictionary
        df_saa_data에서 각 컬럼이 어떤 값인지 매칭 하는 역할
        e.g) {'col_growth' : 성장 관련 컬럼명, 'col_rates' : 금리(물가) 관련 컬럼명}

    convert : bool
        채권 Index의 경우 convert = True 로 설정하여, 금리와 대응.
        default는 False.

    '''
    kuniv_result.index = pd.to_datetime(kuniv_result.index)
    #startDay = kuniv_result.index.min()

    # 재정의
    hmmtag_all = pd.DataFrame(index = kuniv_result.index)

    growth_col = cols_dict['col_growth']
    rates_col = cols_dict['col_rates']

    hmmtag_all[growth_col] = np.nan
    hmmtag_all[rates_col] = np.nan


    hmmtag_all.ix[kuniv_result['State_Name']=='Goldirocks', growth_col] = 1
    hmmtag_all.ix[kuniv_result['State_Name']=='Goldirocks', rates_col] = 0

    hmmtag_all.ix[kuniv_result['State_Name']=='Inflation', growth_col] = 1
    hmmtag_all.ix[kuniv_result['State_Name']=='Inflation', rates_col] = 1

    hmmtag_all.ix[kuniv_result['State_Name']=='Recession', growth_col] = 0
    hmmtag_all.ix[kuniv_result['State_Name']=='Recession', rates_col] = 0

    hmmtag_all.ix[kuniv_result['State_Name']=='Stagflation', growth_col] = 0
    hmmtag_all.ix[kuniv_result['State_Name']=='Stagflation', rates_col] = 1

    if convert :
        hmmtag_all[rates_col] =  (hmmtag_all[rates_col] * -1) + 1

    # plot
    fig, ax_lst = plt.subplots(nrows = 4, sharex= True, figsize =[18,7])

    df_saa_data.ix[startDay:endDay,growth_col].plot(ax = ax_lst[0],color='firebrick')
    df_saa_data.ix[startDay:endDay,rates_col].plot(ax = ax_lst[1],color='green')


    colordict_state_cycle ={
                            0:{'color':'blue', 'alpha': .1, 'ylim':[0,1]}, # Stagflation
                            1:{'color':'pink', 'alpha': .4, 'ylim':[0,1]}}  # Inflation

    hmmtag_all = hmmtag_all.ix[startDay:endDay]
    plot_state(hmmtag_all[growth_col], ax2= ax_lst[0], color_alpha_dict = colordict_state_cycle)
    plot_state(hmmtag_all[rates_col], ax2= ax_lst[1], color_alpha_dict = colordict_state_cycle)


    #주가 : 1이 상승, 0(-1)이 하락때
    #금리 : 1이 상승, 0(-1)이 하락일때

    hmmtag_all2 = hmmtag_all.replace(0, -1)
    state = hmmtag_all2[growth_col].astype(int).astype(str) + '_' + (hmmtag_all2[rates_col]).astype(int).astype(str)
    state = state.replace({'-1_-1' : -1, '-1_1': -2, '1_-1': +0, '1_1': +1}) # MA 할때위해 -2~+1까지로 한정. (0이 나오지않게 하기 위함)

    ####################################
    # noise 필터링.
    # case1) MA.
    #state = state.rolling(20).mean().dropna()

    # case2) rolling 하며, 다빈도 값으로 replace.
    temp_state = state.reset_index().melt(id_vars = ['Date'], value_vars = [0])
    temp_state['cnt'] = 1
    temp_state = temp_state.pivot(index = 'Date', columns ='value', values = 'cnt').fillna(0)
    temp_state = temp_state.rolling(40).mean().idxmax(axis=1)
    state = temp_state.dropna()

    ####################################

    state.ix[state >= -0.5] = state.ix[state >= -0.5]  + 1 # -2~+1 --> -2~+2


    state = state.ix[startDay:endDay]

    state.plot(color='purple', ax = ax_lst[2],label='Cycle_level',linestyle ='--')
    ax_lst[2].fill_between(state.index, state, [0],color='purple',alpha=.1 )
    state = state.round()  #MA의 반올림

    colordict_state_cycle_temp = {-2: {'alpha': 0.1, 'color': 'blue', 'ylim': [0, 1]},
                                 -1: {'alpha': 0.4, 'color': 'gray', 'ylim': [0, 1]},
                                 2: {'alpha': 0.4, 'color': 'pink', 'ylim': [0, 1]},
                                 1: {'alpha': 0.1, 'color': 'yellow', 'ylim': [0, 1]}}

    plot_state(state, ax2= ax_lst[2],color_alpha_dict = colordict_state_cycle_temp)

    color = ['yellowgreen', 'khaki', 'lightcoral']
    kuniv_result.plot(stacked=True, kind='area', ax = ax_lst[3], color = color,alpha=.5,
                      legend = 'reverse')




def SAA_BackTest(df_saa_ratio, pricing, bm_ratio_dict = {'Equity' : [0.33], 'Bond' : [0.33], 'AI' : [0.33]}) :

    # : Define Input :
    # - timetable
    timetable = make_Timetable(pricing , rebalancing_period = 1)
    timetable['isreb'] = np.nan

    # - bm_ratio
    bm_ratio = pd.DataFrame.from_dict(bm_ratio_dict)

    # - ret
    pricing_tN = get_Ret_based_timetable(pricing, timetable)

    # rebalancing period check
    startDate = timetable['From'].iloc[0]
    LastDate = timetable['From'].iloc[-1]
    while startDate < LastDate :
        timetable.ix[timetable['From'] == startDate, 'isreb'] = 1
        startDate = timetable.ix[timetable['From'] == startDate, 'To'].iloc[0]

    pricing_tN['isreb'] = timetable.dropna().set_index('From')['isreb'] # set_index('predFor')? 'Date'?

    # : ratio * ret
    # 1) dynamic (SAA)
    inter_collst = pricing_tN.columns[pricing_tN.columns.isin(df_saa_ratio.columns)]
    ret_agg = df_saa_ratio[inter_collst].mul(pricing_tN[inter_collst])
    tempbool = pricing_tN['isreb'] == 1
    tempbool_idx = tempbool[tempbool].index
    ret_agg = ret_agg.ix[tempbool_idx].dropna()

    # 2) static
    bm_startday = ret_agg.index.min()
    ret_agg_bm = pricing_tN[inter_collst] * bm_ratio[inter_collst].values[0]
    ret_agg_bm = ret_agg_bm.ix[pricing_tN['isreb'] == 1]
    ret_agg_bm = ret_agg_bm.ix[bm_startday:]

    # cumret
    cumret = np.exp(ret_agg.sum(axis=1).cumsum())
    cumret_bm = np.exp(ret_agg_bm.sum(axis=1).cumsum())


    # plot
    fig, ax = plt.subplots(nrows=2, figsize = [12,4])
    cumret.plot(marker='.', color='red', ax = ax[0], label = 'SAA')
    cumret_bm.plot(marker='.', color='black', ax = ax[0],linestyle='--', label = str(bm_ratio_dict))
    ax[0].legend()

    plt.tight_layout()

    color = ['yellowgreen', 'khaki', 'lightcoral']
    df_saa_ratio.plot(stacked=True, kind='area', ax = ax[1], color = color,alpha=.5,
                           legend = 'reverse')







def SAA_main(company_code, product_code, method_type, start_date, end_date, rebalancing_period, convert=True, plot=False, economic=True):
    universe = company_code + '/' + product_code
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- SAA_main: " + method_type + " " + universe)

    '''
    SAA_main(raw_data,method_type,start_day,rebalancing_period,universe,convert=True,plot=False,economic=True)
    SAA.py에서 사용하는 함수를 제어합니다.

    -------
    Parameters

    df_data : DataFrame
        asset에 대해서 선정된 대표index의 price
    method_type : str
        Asset allocation에 사용할 모델명 입니다.
    start_day : str
        backtest에서 시작되는 날짜 입니다.
    rebalancing_period : int
        Portfolio에서 사용되는 rebalancing 주기입니다.
    universe : str
        사용하는 universe의 이름입니다. 경로를 설정하기 위해 사용됩니다.
    convert : Boolean
        convert사용여부를 결정합니다.
    plot : Boolean
        plot표시여부를 결정합니다.
    economic : Boolean
        경제지표 데이터 활용여부를 결정합니다.

    -------
    Returns

    df_saa_ratio : DataFrame
        SAA_chart_allocation_ratio에 의해 생성된 DataFrame입니다. Port_value에서 asset에 대한 ratio가 사용되기 때문에 사용됩니다.

    '''

    # SAA에 사용될 경기/금리 raw 데이터 호출
    df_saa_data = get_saa_raw_data(company_code, product_code)
    df_saa_data['Equity']= df_saa_data['Equity'].apply(lambda x: str(x).replace(',',''))
    df_saa_price = get_saa_price(company_code, product_code)
    df_saa_data = df_saa_data.iloc[:,:2]
    # SAA에 사용될 표준 비율 호출
    df_saa_standard_ratio = get_standard_ratio_data(company_code, product_code)
    # SAA에 사용될 경제지표 호출
    df_economic_data = get_economic_data()

    df_saa_state = SAA_make_business_cycle(df_saa_data, df_economic_data, method_type, start_date, rebalancing_period, universe, convert, economic)
    df_saa_ratio = SAA_get_allocation_ratio(df_saa_standard_ratio,df_saa_state,method_type,universe,economic)
    if plot:
        # df_saa_state = df_saa_state.replace(2,'Inflation').replace(1,'Goldirocks').replace(-1,'Recession').replace(-2,'Stagflation')
        # SAA_chart_business_cycle(df_saa_state, df_saa_data.loc[start_date:])
        # SAA_chart_allocation_ratio(df_saa_ratio)
        SAA_Chart_CycleLevel(df_saa_ratio.dropna(), df_saa_data.iloc[:, :2], startDay = start_date, endDay = end_date,
                             cols_dict={'col_growth': df_saa_data.columns[0], 'col_rates': df_saa_data.columns[1]},
                             convert=False)
        bm_ratio_dict = {'Equity' : [0.5], 'Bond' : [0.5], 'AI' : [0]}
        df_saa_data = df_saa_data.rename(columns = {'ACWI':'Equity', 'GBI':'Bond'})
        SAA_BackTest(df_saa_ratio = df_saa_ratio[start_date:end_date], pricing = df_saa_price, bm_ratio_dict = bm_ratio_dict)


    return df_saa_ratio[start_date:end_date]
