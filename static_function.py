import pandas as pd
import numpy as np
import pywt
import copy
from dateutil.relativedelta import relativedelta
import glob
from collections import OrderedDict
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

def get_time_series_data(country, name):
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " get_time_series_data :" + country + "_" + name)

    df_data = pd.read_csv('Input/' + country + '_' + name + '.csv', encoding='utf-8',
                names=['Date', 'Symbol', 'Open', 'High', 'Low', 'Close',
                       'Volume'], skiprows=1)
    df_data['Date'] = pd.to_datetime(df_data['Date'].index, format='%Y%m%d')
    df_data = df_data.set_index(['Date', 'Symbol']).sort_index()
    print("start date of Data : " + df_data.index.get_level_values('Date')[0].strftime("%Y-%m-%d"))
    print("end date of Data : " + df_data.index.get_level_values('Date')[-1].strftime("%Y-%m-%d"))
    return df_data


def get_universe_data(company_code, product_code):
    df_universe = pd.read_csv('input/' + company_code + '_' + product_code + '_' + 'Universe.csv').transform(
        lambda v: v.ffill().bfill())
    return df_universe


def get_saa_raw_data(company_code, product_code):
    df_saa_data = pd.read_csv('Input/' + company_code + '_' + product_code + '_' + 'SAA_data.csv', parse_dates=True,
                              index_col=0)
    return df_saa_data

def get_saa_price(company_code, product_code):
    df_saa_data = pd.read_csv('Input/' + company_code + '_' + product_code + '_' + 'SAA_price.csv', parse_dates=True,
                              index_col=0)
    return df_saa_data

def get_standard_ratio_data(company_code, product_code):
    df_saa_standard_ratio = pd.read_csv('Input/' + company_code + '_' + product_code + '_' + 'SAA_Standard_ratio.csv')
    return df_saa_standard_ratio


def get_economic_data():
    return pd.read_csv('Input/fredidx.csv', parse_dates=True, index_col=0)


def df_divide(df_data,col_name,df_universe):
    print ("df_divide")
    """
        Return open, high, low, close, volume이 있는 데이터에서 원하는 column만 시계열 데이터로 만든다.


        Parameters
        ----------
        data : DataFrame
            종목별 정보 데이터
        col_name : str
            원하는 정보의 column_name


        Returns
        -------
        df_temp.sort_index() : DataFrame
            col_name의 시계열 데이터
    """
    data=pd.DataFrame(0, index=set(df_data.index.get_level_values('Date')),columns=df_universe.Ticker)
    temp=df_data[col_name]
    for t in df_universe.Ticker:
        data[t]=(temp[:,t]).reset_index().drop_duplicates(['Date']).set_index('Date')
    return data.sort_index()


def get_operation_date(df_data, start_date, end_date):
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " get_operation_date :" + start_date + ":" + end_date)
    date_list = list(OrderedDict.fromkeys(np.array(df_data.index.get_level_values('Date').tolist())))
    start_date = datetime.strptime(start_date, '%Y-%m-%d')
    end_date = datetime.strptime(end_date, '%Y-%m-%d')
    dates = [date for date in date_list if start_date <= date <= end_date]
    return dates, date_list


def get_next_date(df_data, base_date, window):
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " get_next_date :" + base_date.strftime("%Y-%m-%d") + ":" + str(window))
    date_list = list(OrderedDict.fromkeys(np.array(df_data.index.get_level_values('Date').tolist())))
    return date_list[(date_list.index(base_date) + int(window))]


def make_interval_date(date_list, start_date, end_date, month_interval = 1) :
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " make_interval_date :" + start_date.strftime("%Y-%m-%d") + ":" + end_date.strftime("%Y-%m-%d"))
    interval_list = []
    while start_date < end_date :
        interval_list.append([date for date in date_list if start_date <= date][0])
        start_date += relativedelta(months = month_interval)
    return interval_list


def make_baseMP_date(date_list_all, date_list, backday= 1) :
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " make_baseMP_date")
    baseMP_list = []
    for date in date_list:
        baseMP_list.append(date_list_all[(date_list_all.index(date) - backday)])
    return baseMP_list


def make_Timetable(rebalancing_period = 1) :
    # - bizday
    bizdate, _ = get_operation_date(pricing, pricing.index.min().strftime('%Y-%m-%d'), pricing.index.max().strftime('%Y-%m-%d'))
    
    # - rebalancing date based on bizday
    t_fwd = rebalancing_period
    timetable = pd.DataFrame(bizdate, columns = ['From'])
    timetable['To'] = timetable.shift(-t_fwd)
    
    return timetable
    
def get_Ret_based_timetable(pricing, timetable) :
     # - price data -> log ret
    pricing_tN = pricing.copy()
    pricing_tN[:] = np.log(pricing.ix[timetable['To']].values / pricing.ix[timetable['From']].values  )
    
    # - index 맞춰주기 : (df_saa_ratio, pricing)
    pricing_tN = pricing_tN.ix[df_saa_ratio.index].fillna(method='ffill')
    
    return pricing_tN


def get_MDD(pricing, time_window) :
    # time table
    timetable = make_Timetable(time_window)
    
    # getret
    temp_ret = get_Ret_based_timetable(pricing, timetable)
    
    # mdd
    temp_mdd = temp_ret[temp_ret<0].copy()
    temp_mdd = temp_mdd.fillna(0)
    temp_mdd = temp_mdd.rolling(999999, min_periods = 1).min()
    
    return temp_mdd, temp_ret


def plot_MDD(pricing, time_n, ax, colormap = plt.cm.get_cmap('jet', 10)) :
    temp_mdd, temp_mdd_ret = get_MDD(pricing, time_window = time_n)
    temp_mdd.plot(ax = ax, alpha=.8, colormap = colormap, linestyle = '--')
    temp_mdd_ret = temp_mdd_ret[temp_mdd_ret<0].fillna(0)
    #temp_mdd_ret.plot(ax = ax_lst[2], alpha=.5, colormap = colormap, label=False)
    
    i_cnt = 1
    for each in temp_mdd_ret.columns :
        temp_color = colorarr(i_cnt/len(temp_mdd_ret.columns))
        ax.fill_between(temp_mdd_ret.index, temp_mdd_ret[each], [0],alpha=.5, color = temp_color)
        i_cnt += 1
    
    ax.set_title('MDD_'+str(time_n))
    plt.tight_layout()


def run_plot_cumret_vol_mdd(pricing, isRet = False) :
    if isRet == False :
        # time table
        timetable = make_Timetable(1)
        # getret
        temp_ret = get_Ret_based_timetable(pricing, timetable)
        cumret = np.exp(temp_ret.cumsum())
        
    # [plot]
    fig, ax_lst = plt.subplots(nrows=4, figsize =[8,9], sharex=True)
    colorarr = plt.cm.get_cmap('jet', 10)
    
    # ax_0 
    cumret.plot(ax = ax_lst[0], marker='.', alpha=.8, colormap = colorarr) #cumret
    ax_lst[0].set_title('CumRet')
    plt.tight_layout()
    
    # ax_1
    temp_ret.plot(ax = ax_lst[1], alpha=.8, colormap = colorarr)
    ax_lst[1].set_title('Vol_Daily')
    i_cnt = 1
    for each in temp_mdd_ret.columns :
        temp_color = colorarr(i_cnt/len(temp_ret.columns))
        ax_lst[1].fill_between(temp_ret.index, temp_ret[each], [0],alpha=.1, color = temp_color)
        i_cnt += 1
    plt.tight_layout()
    
    # ax_2
    plot_MDD(pricing, time_n = 5, ax = ax_lst[2])
    
    # ax_3
    plot_MDD(pricing, time_n = 20, ax = ax_lst[3])











