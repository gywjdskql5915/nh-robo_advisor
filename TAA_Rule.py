import numpy as np
import pandas as pd
import os
from os import listdir
from Wavelet import *
from datetime import datetime


# <TAA Rule-based Part>
# 00. Wavelet으로 smoothing
def get_wt_preprocessing(df_data, ticker):
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- get_wt_preprocessing :" + ticker)
    df=df_data.values
    edata = list(df[:100])
    #edata = list((WT(df[:100], lv=7, n=7,m=4))[0])
    for i in range(100, len(df)):
        edata.append(((WT(df[:i+1], lv=7, n=7,m=4))[0])[-1])
    return pd.Series(edata,index=df_data.index)


def smoothing_price_dic(df, df_universe):
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- smoothing_price_dic")

    """
        Wavelet Transform으로 smoothing 된 종가를 dictionary 형태로 출력합니다.


        Parameters
        ----------
        df_close : DataFrame
            종가
        start_date : str
            Backtesting 시작일 “2017-01-01”

        Returns
        -------
        smooted_price : Dictionary
            Wavelet Transform으로 smoothing 된 종가
            key: 날짜, value: smoothed 된 종가 DataFrame
    """
    close = dict()
    for ticker in df_universe.Ticker:
        close[ticker]=get_wt_preprocessing(df[ticker], ticker)
    close_wt = pd.concat([pd.Series(close[ticker], index=df.index) for ticker in df_universe.Ticker], axis=1)
    close_wt.columns = df_universe.Ticker
    return close_wt

def smoothing(df_close,universe,start_date, df_universe):
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- smoothing")
    """
        Smoothed된 데이터를 불러옵니다.


        Parameters
        ----------
        df_close : DataFrame
            종가
        universe : str
            데이터 유니버스 'K01'
        df_universe : DataFrame
        start_date : str
            Backtesting 시작일 “2017-01-01”

        Returns
        -------
        smooted_price : Dictionary
            Wavelet Transform으로 smoothing 된 종가
            key: 날짜, value: smoothed 된 종가 DataFrame
    """
    if 'smoothed_price.csv' in listdir(os.getcwd()+'/'+universe+ '/Intermediate' ):
        print('smoothed_price file exists')
        smoothed_price = pd.read_csv(universe + '/Intermediate/smoothed_price.csv',parse_dates = True, index_col = 0)
    else:
        print('smoothed_price file does not exist')
        smoothed_price = smoothing_price_dic(df_close, df_universe)
        smoothed_price.to_csv(universe + '/Intermediate/smoothed_price.csv')
    return smoothed_price

# 1. NH 방식
def NH(dates, smoothed_price,df_universe, threshold=3):
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- NH")
    """
         기존 NH에서 제시한 Dual Momentum 전략. 특정 시점 기준으로 1, 2, 3, 4, 5, 10, 20, 40, 60, 120 일
         전에 비해 상승하면 1, 하락하면 0으로 계산한 score를 합산한다.


         Parameters
         ----------
         dates : string
             backtesting 하는 날짜
         smoothed_price : dictionary
             Wavelet Transform으로 smoothing 된 종가
         df_universe : DataFrame
             Backtest에 사용하는 데이터 유니버스
         threshold : float
             Momentum score의 threshold (threshold 보다 작은 momentum score들은 0)

         Returns
         -------
         Momentum * (Momentum >= threshold).astype('int') : DataFrame
             백테스팅 기간동안 Momentum threshold보다 큰 값들만 DataFrame 형태로 출력
     """
    lookback_days = [1, 2, 3, 4, 5, 10, 20, 40, 60, 120]
    Momentum=pd.DataFrame(0,index=dates, columns=df_universe.Ticker)
    temp = sum([(smoothed_price > smoothed_price.shift(i)).astype('int') for i in lookback_days])
    for date in dates:
        Momentum.loc[date] = temp.loc[date]

    return Momentum * (Momentum >= threshold).astype('int')

# 2. Dual Momentum 방식
def DM(dates, smoothed_price,df_universe,momentum_window=40, decay=4):
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- DM")

    """
        개선된 Dual Momentum 전략. 특정 시점 기준으로 momentum_window 일
        전과 비교되는 값을 이용하여  score를 계산한다.


        Parameters
        ----------
        dates : string
            backtesting 하는 날짜
        smoothed_price : dictionary
            Wavelet Transform으로 smoothing 된 종가
        df_universe : DataFrame
            Backtest에 사용하는 데이터 유니버스
        momentum_window : int
            특정일 기준으로 momentum score을 계산할 일 수
        decay :
            Momentum socre가 exponentially weighted 되는데, 예를 들어, decay 가 4이면, 초기 weight에 비해 최종 weight는 1/4 값이다.

        Returns
        -------
        Momentum : DataFrame
            백테스팅 기간동안 개선된 Dual Momentum score을 DataFrame 형태로 출력
    """

    Momentum = pd.DataFrame(0, index=dates, columns=df_universe.Ticker)
    decay = np.log(decay) / momentum_window
    ew_series = np.array([np.exp(-i * decay) for i in range(1, momentum_window + 1)])
    ew_series /= ew_series.sum()
    temp = sum([ew_series[i] * (smoothed_price - smoothed_price.shift(i)) / smoothed_price \
                * (smoothed_price > smoothed_price.shift(i)).astype('int') for i in range(1, momentum_window)])
    for date in dates:
        Momentum.loc[date] = temp.loc[date]
    return Momentum