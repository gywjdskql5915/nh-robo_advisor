import numpy as np
import random
from datetime import datetime
np.random.seed(42)
random.seed(42)

import TAA_Tech as ta
import pandas as pd
from sklearn.ensemble import RandomForestClassifier

from sklearn.metrics import f1_score, precision_score, confusion_matrix, recall_score, accuracy_score
from Wavelet import *

def feature_extraction(data):
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- feature_extraction")
    """
        기술적 지표들을 생성하는 함수입니다.


        Parameters
        ----------
        data : dataframe
            OLHC+V 데이터


        Returns : dataframe
            feature들(technical indictors)이 추가된 테이터
        -------

    """
    list_period=[5, 10, 20, 40, 80]
    for x in list_period:
        data = ta.relative_strength_index(data, n=x)
        data = ta.stochastic_oscillator_d(data, n=x)
        data = ta.accumulation_distribution(data, n=x)
        data = ta.average_true_range(data, n=x)
        data = ta.momentum(data, n=x)
        data = ta.money_flow_index(data, n=x)
        data = ta.rate_of_change(data, n=x)
        data = ta.on_balance_volume(data, n=x)
        data = ta.commodity_channel_index(data, n=x)
        data = ta.ease_of_movement(data, n=x)
        data = ta.trix(data, n=x)
        data = ta.vortex_indicator(data, n=x)
        data = ta.exponential_moving_average(data, n=x)
        #data = ta.keltner_channel(data, n=x)
        #data = ta.coppock_curve(data, n=x)

    data = ta.macd(data, n_fast=12, n_slow=26)
    data = ta.slope(data, 20, 40)
    #data = ta.slope(data, 10, 20)
    #data = ta.slope(data, 5, 10)
    #data = ta.slope(data, 2, 5)

    del (data['Open'])
    del (data['High'])
    del (data['Low'])
    del (data['Volume'])

    return data.transform(lambda v: v.ffill()).bfill(), list_period[-1]


def compute_prediction_int(df, n):
    """
        n 일 뒤 종가 상승여부 (오르면 1, 내리면 0)를 출력


        Parameters
        ----------
        df : DataFrame
            Input data
        n : int
            특정시점 기준으로 예측할 미래일

        Returns
        -------
        (pred > 0).astype('int') : DataFrame
            n 일 뒤 종가 상승여부 (오르면 1, 내리면 0)
    """

    pred = (df.shift(-n)['Close'] - df['Close'])
    pred = pred.fillna(0)

    return (pred > 0).astype('int')


def prepare_data(df, horizon):
    """
        데이터를 모델에 입력하기 좋게 가공하는 함수입니다.


        Parameters
        ----------
        df : dataframe
            OLHCV의 데이터
        horizon : int
            몇일 뒤를 예측할 것인지를 나타내는 숫자


        Returns : dataframe
        -------

    """
    #lambda 함수
    data, period = feature_extraction(df)
    data=data.transform(lambda v: v.ffill()).bfill()
    data['pred'] = compute_prediction_int(data, n=horizon)
    #data['yesterday_return'] = data['Close_raw']-data['Close_raw'].shift(1)
    del (data['Close'])
    del (data['Close_raw'])
    return data.transform(lambda v: v.ffill().bfill()), period



def ML(df_close, df_volume, df_open, df_high,df_low,start_date, tickers):
    #print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- ML")
    """
        Machine Learning(Random Forest)의 방식을 활용한 스코어를 산출하는 함수입니다.


        Parameters
        ----------
        df_close : dataframe

        df_volume : dataframe

        df_open : dataframe

        df_high : dataframe

        df_low : dataframe

        start_date : datetime
            백테스트 시작일
        tickers : list
            유니버스 내의 모든 종목 리스트


        Returns : dataframe
            각 종목의 점수
        -------

    """

    Close = df_close.reset_index()
    Open = df_open.reset_index()
    High = df_high.reset_index()
    Low = df_low.reset_index()
    Volume = df_volume.reset_index()

    index_date = (df_close.index)
    print(index_date[-1])
    prediction = {}
    df_added = {}
    last = ''
    S = {}
    rebalance_period =20
    for t in tickers:
        temp = pd.DataFrame(0, columns=['Close', 'Open', 'High', 'Low', 'Volume'], index=Close.index)
        temp['Close'] = get_wt_preprocessing(Close[t])
        temp['Close_raw'] = Close[t].apply(remove_points)
        temp['Open'] = Open[t].apply(remove_points)
        temp['High'] = High[t].apply(remove_points)
        temp['Low'] = Low[t].apply(remove_points)
        temp['Volume'] = Volume[t].apply(remove_points)
        temp = temp.bfill().ffill().dropna()
        df_added[t], period = prepare_data(temp, 20)

        S[t] = (df_added[t]['Slope_40']).values
        y = df_added[t]['pred']
        features = [x for x in df_added[t].columns if x not in ['gain', 'pred','Slope_40']]
        X = df_added[t][features]
        X[X == np.inf] = np.nan
        X[X == -np.inf] = np.nan
        X = X.transform(lambda v: v.ffill()).bfill()
        y = y.transform(lambda v: v.ffill()).bfill()
        train_size = len(df_close.loc[:start_date]) - 1- rebalance_period

        X_train = X[period:train_size]
        X_test = X[train_size+rebalance_period :]

        y_train = y[period:train_size]
        y_test = y[train_size+rebalance_period :]
        num_trees = 65

        rf = RandomForestClassifier(n_jobs=-1, n_estimators=num_trees, random_state=42, warm_start=True)
        rf.fit(X_train, y_train.values.ravel());
        prediction[t] = rf.predict((X_test))

        last = t

        pred = prediction[t]
        y_test = [1 if t > 0.0 else 0 for t in y_test[:-20]]

        pred_for_test=[int(s+p>0) for s,p in zip((S[t])[-len(pred[:-20])-20:-20],pred[:-20])]
        precision = precision_score(y_pred=pred_for_test, y_true=y_test)
        recall = recall_score(y_pred=pred_for_test, y_true=y_test)
        f1 = f1_score(y_pred=pred_for_test, y_true=y_test)
        accuracy = accuracy_score(y_pred=pred_for_test, y_true=y_test)
        confusion = confusion_matrix(y_pred=pred_for_test, y_true=y_test)
        print(t)
        print(len(prediction[t]))
        print('precision: {0:1.2f}, recall: {1:1.2f}, f1: {2:1.2f}, accuracy: {3:1.2f}'.format(precision, recall, f1,
                                                                                   accuracy))
        print(confusion)
        print()

    result = pd.DataFrame(prediction, index=index_date[-len(prediction[last]):], columns=prediction.keys())
    Slope = pd.DataFrame(S, index=index_date[-len(S[last]):], columns=S.keys())
    score = result + (Slope.loc[result.index[0]:result.index[-1]])
    score *= (score>0)
    #score.to_csv('score.csv')
    return score