from TAA_ML import *
from TAA_Rule import *
from Wavelet import *
from datetime import datetime
from static_function import *
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler
import numpy as np
from scipy import stats
from sklearn.metrics import f1_score, precision_score, confusion_matrix, recall_score, accuracy_score

def make_hybrid_score(score_ml, score_dm, df_percentage,threshold = 0.5):
    score = df_percentage.copy()

    score_ml = score_ml.apply(lambda x: (x-score_ml.mean(axis=1))/score_ml.std(axis=1))
    score_dm = score_dm.apply(lambda x: (x-score_dm.mean(axis=1))/score_dm.std(axis=1))

    for i in (df_percentage.index):
        score.loc[i] =  (score_ml.loc[i])*(df_percentage.loc[i]<=threshold) + \
                        (score_dm.loc[i]) * (df_percentage.loc[i] > threshold)
    return score

def make_percentage(start_date, score, close, universe):
    df_percentage = pd.DataFrame(0, index=close[:"2016-01-01"].index, columns=score.columns)
    close = close.loc[score.index[0]:]
    for i in score[start_date:].index:
        print(i)
        y_pred = (score.loc[:i] > 0.002).astype(int)
        y_true = (close.loc[:i].pct_change(20).shift(-20) > 0).astype(int)
        y_false = (close.loc[:i].pct_change(20).shift(-20) <= 0).astype(int)
        tp = y_pred * y_true
        fp = y_pred * y_false
        precision = tp.sum() / (tp.sum()+fp.sum())
        df_percentage.loc[i] = precision
        '''
        for t in score.columns:
            y_pred = (score.loc[:i] > 0.002).astype(int)[t]
            y_true = (close.loc[:i].pct_change(20).shift(-20) > 0).astype(int)[t]

            precision = precision_score(y_pred=y_pred, y_true=y_true.loc[y_pred.index])
            print(precision)
            df_percentage[t].loc[i] = precision
        '''
    df_percentage.to_csv(universe+'/Intermediate/df_percentage.csv')
    return df_percentage


def TAA_main(company_code, product_code, taa_method, start_date, end_date, df_data, df_universe):
    universe = company_code + '/' + product_code
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- TAA_main : " + taa_method + " " + universe)
    """
        TAA 방식에 따른 score을 출력합니다.


        Parameters
        ----------
        taa_method : string
            TAA 방법 (NH, DM, ML, Hybrid)
        df_data : DataFrame
            Backtest에 사용하는 데이터
        df_data : DataFrame
            날짜별 종목별 weight “df_weight”
        start_date : str
            Backtesting 시작일 “2017-01-01”
        end_date : str
            Backtesting 종료일 “2018-12-30”
        universe : str
            데이터 유니버스 'K01'
        df_universe : DataFrame
            Backtest에 사용하는 데이터 유니버스


        Returns
        -------
        score.mul(df_selected) : DataFrame
            TAA 방식에 따른 score
    """
    df_close = pd.merge(df_data.reset_index(), df_universe, left_on='Symbol', right_on='Ticker', how='inner')
    df_close = df_close.pivot('Date','Symbol','Close')
    df_close.to_csv('df_close.csv')
    dates = df_close.loc[start_date:end_date].index

    if taa_method == 'NH':
        smoothed_price = smoothing(df_close,universe,start_date, df_universe)
        score = NH(dates, smoothed_price,df_universe)

    elif taa_method == 'DM':
        smoothed_price = smoothing(df_close,universe,start_date, df_universe)
        score = DM(dates, smoothed_price,df_universe )  # momentum_threshold
        score.to_csv('score_dm.csv')
        #score_perform= fun1(score)


    elif taa_method == 'ML':
        if 'ml_score' + '.csv' in listdir(os.getcwd() + '/' + universe + '/Intermediate'):
            print('ml_score file exists')
            ml_score = pd.read_csv(universe + '/Intermediate/ml_score.csv', parse_dates=True, index_col=0)
        else:
            print('ml_score file does not exist')
            df_open = pd.merge(df_data.reset_index(), df_universe, left_on='Symbol', right_on='Ticker', how='inner')
            df_open = df_open.pivot('Date', 'Symbol', 'Open')

            df_high = pd.merge(df_data.reset_index(), df_universe, left_on='Symbol', right_on='Ticker', how='inner')
            df_high = df_high.pivot('Date', 'Symbol', 'High')

            df_low = pd.merge(df_data.reset_index(), df_universe, left_on='Symbol', right_on='Ticker', how='inner')
            df_low = df_low.pivot('Date', 'Symbol', 'Low')

            df_volume = pd.merge(df_data.reset_index(), df_universe, left_on='Symbol', right_on='Ticker', how='inner')
            df_volume = df_volume.pivot('Date', 'Symbol', 'Volume')

            ml_score = ML(df_close, df_volume, df_open, df_high, df_low, start_date, df_universe.Ticker)
            ml_score.to_csv(universe + '/Intermediate/ml_score.csv')
        ml_score *= (ml_score >= 0).astype('int')
        score = ml_score
    elif taa_method == 'Hybrid':
        smoothed_price = smoothing(df_close, universe, start_date, df_universe)
        momentum_score = DM(df_close.index, smoothed_price, df_universe)
        if 'df_percentage.csv' in listdir(os.getcwd() + '/' + universe + '/Intermediate'):
            print('df_percentage file exists')
            df_percentage = pd.read_csv(universe + '/Intermediate/df_percentage.csv', parse_dates=True, index_col=0)
        else:
            print('df_percentage file does not exists')
            df_percentage = make_percentage(start_date, momentum_score, df_close, universe)

        if 'ml_score.csv' in listdir(os.getcwd() + '/' + universe + '/Intermediate'):
            print('ml_score file exists')
            ml_score = pd.read_csv(universe + '/Intermediate/ml_score.csv', parse_dates=True, index_col=0)
        else:
            print('ml_score file does not exist')
            df_open = pd.merge(df_data.reset_index(), df_universe, left_on='Symbol', right_on='Ticker', how='inner')
            df_open = df_open.pivot('Date', 'Symbol', 'Open')

            df_high = pd.merge(df_data.reset_index(), df_universe, left_on='Symbol', right_on='Ticker', how='inner')
            df_high = df_high.pivot('Date', 'Symbol', 'High')

            df_low = pd.merge(df_data.reset_index(), df_universe, left_on='Symbol', right_on='Ticker', how='inner')
            df_low = df_low.pivot('Date', 'Symbol', 'Low')

            df_volume = pd.merge(df_data.reset_index(), df_universe, left_on='Symbol', right_on='Ticker', how='inner')
            df_volume = df_volume.pivot('Date', 'Symbol', 'Volume')

            ml_score = ML(df_close, df_volume, df_open, df_high, df_low, start_date, df_universe.Ticker)
            ml_score.to_csv(universe + '/Intermediate/ml_score.csv')

        ml_score *= (ml_score >= 0).astype('int')
        score = make_hybrid_score(ml_score, momentum_score, df_percentage.loc[start_date:],threshold = 50)
        score.to_csv('score.csv')
    else:
        print("Select Scoring Method: NH, DM, ML, Hybrid")
    df_selected = pd.DataFrame(0, columns=score.columns, index=score.index)
    # 속도 느린구간. 개선 필요
    for i in score.index:
        for m in set(df_universe.Mid):
            if max(score[(df_universe[df_universe.Mid == m]).Ticker].loc[i]) > 1e-6:
                selected = np.argmax(score[df_universe[df_universe.Mid == m].Ticker].loc[i])
                df_selected[selected].loc[i] = 1
    #
    score.mul(df_selected).to_csv(universe + '/Intermediate/df_selected_score.csv')
    return score.mul(df_selected)

