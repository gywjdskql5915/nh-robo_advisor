import pandas as pd


def make_MP(df_weight, baseMP_list, interval_list, company_code, product_code, country):
    df_mp = df_weight.reset_index().melt(id_vars='index', var_name="Ticker", value_name='Temp_score')
    df_mp = df_mp[df_mp['Temp_score'] >= 0.01].sort_values(['index', 'Ticker'])
    df_mp['Score'] = ((df_mp['Temp_score'] * 10000).astype('int') / 100)
    del df_mp['Temp_score']
    df_mp = df_mp.rename(columns={'index': 'BaseDate', 'Ticker': 'Symbol', 'Score': 'Ratio'})
    df_mp_all = pd.DataFrame()
    for date in baseMP_list:
        df_mp_day_sum = pd.DataFrame()
        df_mp_day = df_mp[df_mp['BaseDate'] == date]
        df_mp_day['CompanyCode'] = company_code
        df_mp_day['ProductCode'] = product_code
        df_mp_day['UniverseCode'] = '02' if country == 'US' else '01'

        df_mp_day['Account'] = '_'
        df_mp_day['AutoMake'] = 'N'
        df_mp_day['CountryCode'] = '200' if country == 'US' else '000'
        df_mp_day['APMake'] = 'Y'
        df_mp_day['Reason'] = ''
        df_mp_day['BaseDate'] = df_mp_day['BaseDate'].dt.strftime("%Y%m%d")
        df_mp_day['RegDate'] = interval_list[baseMP_list.index(date)].strftime("%Y%m%d")
        # df_mp_day['BaseDate'] = df_mp_day['RegDate']
        for tendency_code in ['01', '02', '03', '04', '05']:
            df_mp_day['TendencyCode'] = tendency_code
            df_mp_day_sum = df_mp_day_sum.append(df_mp_day)
        df_mp_day_sum = df_mp_day_sum[['RegDate', 'CompanyCode', 'ProductCode', 'Account', 'TendencyCode', 'AutoMake', 'UniverseCode', 'CountryCode', 'Symbol',
                      'Ratio', 'BaseDate', 'Reason','APMake']]
        df_mp_day_sum.sort_values(by=['Symbol', 'TendencyCode']).to_csv(
            './' + company_code + "/" + product_code + '/MP/' + date.strftime("%Y%m%d") + ".csv", index=False,
            header=False, encoding='euc-kr')
        df_mp_all = df_mp_all.append(df_mp_day_sum)
    return df_mp_all


