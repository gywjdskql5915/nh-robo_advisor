# NH-robo_advisor

NH 산학협력 프로젝트 (로보어드바이저)

## Quick Tour

알고리즘 정리.pdf 

    -> 로보어드바이저 전체 개괄 설명

Input 

    -> 데이터 (한국 / 미국)

FANH01 (K01-한국/K02-미국)

    -> Intermediate :  (중간결과값들 - 체크용)
    -> MP : 거래 체결 내용 (계좌 내역) 
    -> Output : 각 조합별 결과 자료

Result 

    -> KR_total : 한국장 백테스트 결과
    -> UT_total : 미국장 백테스트 결과

SAA 

    -> SAA.py : SAA 전체 구동
    -> SAA_make_data.py : SAA 분석에 필요한 데이터 생성
    -> SAA_make_model.py : SAA 분석 모델

TAA 

    -> TAA.py : TAA 전체 구동
    -> TAA_Rule.py : Momentum 방식
    -> TAA_ML.py : ML 방식
    -> TAA_Tech.py : ML 방식을 분석하는데 필요한 기술적 지표 계산 
    -> Wavelet.py : Wavelet transform 
OPT
    
    -> OPT.py : Risk Parity 및 Hierarchical Risk Parity 

BACKTEST 관련

    -> Main.py : 전체 구동 및 결과값 반환 (전체 백테스팅 계산)
    -> Main_total.py : 한국/미국 전체 구동
    -> MP : 계좌 체결 상태 계산
    -> SIMUL : 실시간으로 계산 (실제 거래상황이랑 비슷하게)


