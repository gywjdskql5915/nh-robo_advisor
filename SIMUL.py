import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from Wavelet import *
from static_function import *

def Backtesting(df_saa_ratio,df_data,portfolio_weight,start_date,end_date,df_universe,rebalancing_period):

    """
        Return Backtest 기간과 데이터 정보, weigth의 시계열 데이터를 받아 backtest한다.


        Parameters
        ----------
        df_saa_ratio : DataFrame
            국면으로 결정지어지는 대분류 weight의 시계열 데이터
        df_data : DataFrame
            Backtest에 사용하는 데이터
        portfolio_weight : DataFrame
            날짜별 종목별 weight “df_weight”
        rebalancing_period : int
            리밸런싱 기간 “20”
        start_date : str
            Backtesting 시작일 “2017-01-01”
        end_date : str
            Backtesting 종료일 “2018-12-30”
        df_universe : DataFrame
            Backtest에 사용하는 데이터 유니버스


        Returns
        -------
        pd.DataFrame.from_dict(portfolio_value, orient='index', columns=['PV'])['PV'] : Series
            portfolio value
    """

    df_close = df_divide(df_data, 'Close',df_universe)
    tickers = df_universe.Ticker
    large_tickers = list(set(df_universe.Large))
    rebal_index = pd.Series((df_close.loc[start_date:end_date].index)[::rebalancing_period], dtype='object')
    #backtest_index = df_close.loc[start_date:end_date].index
    backtest_index =pd.Series(df_close.loc[start_date:end_date].index, dtype='object')
    eps = 1.0e-5

    new_index = pd.date_range(start=df_saa_ratio.index[0], end=df_close.index[-1], freq='D')
    new_df_saa_ratio = df_saa_ratio.reindex(new_index, method='ffill')


    #new_df_saa_ratio = df_saa_ratio.reindex(
    #    set(df_saa_ratio.index.append(pd.to_datetime(rebal_index)))).sort_index().fillna(method='bfill')

    cash_weight = dict()
    holdings = dict()
    cash_value = dict()
    portfolio_value_df = dict()
    portfolio_value = dict()
    # for loop
    for i, date in enumerate(backtest_index):
        if i == 0:
            cash_value[date] = 1
            holdings[date] = pd.Series(0, index=tickers)
            portfolio_value[date] = (holdings[date] * (df_close).loc[date]).sum() + cash_value[date]


        else:
            prev_date = backtest_index[i - 1]

            if date not in rebal_index.values:
                cash_value[date] = cash_value[prev_date]
                holdings[date] = holdings[prev_date]
                portfolio_value[date] = (holdings[date] * df_close.loc[date]).sum() + cash_value[date]

            elif date in rebal_index.values:
                # SAA

                large_weight = new_df_saa_ratio[large_tickers].loc[date]

                non_zero_large_cat = 0

                portfolio_weight_copy = pd.Series(portfolio_weight.loc[date], index=tickers)

                for large_cat in large_tickers:
                    if portfolio_weight_copy[(df_universe[df_universe.Large==large_cat]).Ticker].sum() > eps:
                        non_zero_large_cat += large_weight[large_cat]
                        #portfolio_weight_copy[(df_universe[df_universe.Large==large_cat]).Ticker] *= large_weight[large_cat] / \
                        #portfolio_weight_copy[(df_universe[df_universe.Large==large_cat]).Ticker].sum()

                #portfolio_weight.loc[date] = portfolio_weight_copy

                cash_weight[date] = 1 - non_zero_large_cat

                portfolio_value[date] = (holdings[prev_date] * df_close.loc[date]).sum() + cash_value[prev_date]

                portfolio_value_df[date] = portfolio_weight.loc[date] * portfolio_value[date]

                cash_value[date] = cash_weight[date] * portfolio_value[date]
                holdings[date] = portfolio_value_df[date] / df_close.loc[date]
    return pd.DataFrame.from_dict(portfolio_value, orient='index', columns=['PV'])['PV']



def MDD(Close, Wavelet=False):

    """
        Return portfolio value의 MaxDrawDown값을 구한다.


        Parameters
        ----------
        Close : Series
            portfolio value 시계열 데이터
        Wavelet : bool
            Wavelet 여부


        Returns
        -------
        result : Series
            MDD
    """
    if (Wavelet):
        data_wt = WT(Close, lv=4, n=4)
        # data_wt = pd.Series(list(data_wt), index=Close.index)
        data = (2 * (data_wt.pct_change(1) > 0).astype('int') - 1).rolling(2).sum().shift(-1)
        index = data[data.values == 0].index[1:] if data.loc[data.index[0]] <= 0 else data[data.values == 0].index
    else:
        data = (2 * (Close.pct_change(1) > 0).astype('int') - 1).rolling(2).sum().shift(-1)
        # print(data)
        index = data[data.values == 0].index[1:] if data.loc[data.index[0]] <= 0 else data[data.values == 0].index

    a = list(Close.loc[index[::2]].values)
    a_index = index[::2]
    b = list(Close.loc[index[1::2]].values)
    b_index = index[1::2]

    result = pd.Series([(aa - bb) for aa, bb in zip(a[:len(b)], b)], index=b_index[:len(b)])

    maxi = 0

    result2 = result.copy()
    for i, r in zip(result.index, result.values):
        if (r < maxi):
            result[i] = maxi
        else:
            maxi = r

    # plt.plot(result)
    # plt.plot(result2, 'x')
    # plt.show()
    return result

def multi_plot(pv_df, taa_method,saa_method,opt_method, universe, start_date,end_date,company_code,product_code,mdd=False, bm=True, SAA_plot=False):
    """
         Return Backtest 결과를 그래프로 그려준다.


         Parameters
         ----------
         pv_df : Series
             portfolio value “df_pv”
         taa_method : str
             NH / DM / ML / Hybrid
         saa_method : str
             HMM / ML / Hybrid / NH_Fred_ML
         universe : str
             데이터 유니버스 'K01'
         start_date : str
             Backtesting 시작일 “2017-01-01”
         end_date : str
             Backtesting 종료일 “2018-12-30”
         mdd : bool
             mdd=False
         bm : bool
             bm=True
         SAA_plot :bool
             SAA_plot=False


         Returns
         -------
         Graph :
             Backtest 결과 그래프
     """
    df_saa_data = get_saa_raw_data(company_code, product_code)

    color = dict()
    color['NH'] = 'b'; color['DM'] = 'g'; color['ML'] = 'r'; color['Hybrid'] = 'c'; color['SAA_HMM'] = 'm'
    color['SAA_ML'] = 'y'; color['SAA_Hybrid'] = 'b'; color['Equal'] = 'g'; color['RB'] = 'r'; color['Bench'] = 'c'
    color=color[taa_method]
    bench = (df_saa_data.sum(axis=1)/3).loc[start_date:end_date]
    bench = bench/bench.iloc[0]
    #
    #
    #date_min, date_max = datetime.datetime.strptime(start_date,'%Y-%m-%d'), datetime.datetime.strptime(end_date,'%Y-%m-%d')
    date_min, date_max = pv_df.index[0],pv_df.index[-1]
    #date_min, date_max = max(min(pv_df.index), min(bench.index)), min(max(pv_df.index), max(bench.index))

    # backtest_index.min(), backtest_index.max()

    pv_min, pv_max = 0.8, 1.45  # min(pv_df.min(axis=0))*0.95 , max(pv_df.max(axis=0))*1.05

    column_names = pv_df.keys()
    column_name = taa_method

    if SAA_plot:
        plt.figure(figsize=(15, 5))
        if economic:
            state = pd.read_csv('{}/state_{}_economic.csv'.format(universe + '/Input', saa_method[4:]), parse_dates=True,
                                index_col='Date')
            state = state[saa_method[4:]]
        else:
            state = pd.read_csv('{}/state_{}.csv'.format(universe + '/Input', saa_method[4:]), parse_dates=True,
                                index_col='Date')
            state = state[saa_method[4:]]
        for i, cr in zip(range(0, 4), ['deepskyblue', 'silver', 'gold', 'lawngreen']):
            if len(state[state == i]) == 0:
                pass
            else:
                plt.bar(state[state == i].index,
                        np.repeat(3, len(state[state == i])), color=cr, alpha=1, width=8)

    else:
        plt.figure(figsize=(15, 8))
    if mdd:
        pv_min, pv_max = 0, 0.7
        bench = MDD(bench).fillna(method='ffill').fillna(0)
        #column_name = pv_df.columns[0]
        pv_df = MDD(pv_df).fillna(method='ffill').fillna(0)
        #pv_df = MDD(pv_df[pv_df.columns[0]]).fillna(method='ffill').fillna(0)
        print('MDD: ',int(max(pv_df.values) * 1000) / 1000)



    else:
        year = (pv_df.index[-1] - pv_df.index[0]).days / 365
        value = float(pv_df.iloc[-1] ** (1 / year) - 1)
        # return int(value * 1000) / 1000
        temp = float(bench.iloc[-1] ** (1 / year) - 1)
        print('PV: ',int(value * 1000) / 1000)

    if bm:
        plt.plot(bench, label='Bench', color='k', linewidth=2)

    # for column_name in column_names:
    plt.plot(pv_df.loc[date_min:date_max], label=column_name, color=color, linewidth=2)

    plt.legend(prop={'size': 16})
    plt.xticks(fontsize=16, rotation=30)
    plt.yticks(fontsize=16)
    plt.xlim(date_min, date_max)
    plt.ylim(pv_min, pv_max)

    plt.xlabel('Date', fontsize=20)
    plt.ylabel('Portfolio Value', fontsize=20)
    if mdd:
        plt.savefig(universe + '/Output/' + saa_method+'_'+ taa_method +'_'+ opt_method+ '_mdd.png')
    else:
        plt.savefig(universe + '/Output/' + saa_method+'_'+ taa_method+'_'+ opt_method + '_pv.png')