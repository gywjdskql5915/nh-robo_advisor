import pandas as pd
from Wavelet import *
from static_function import *

def prob_drop(df_close, ticker):

    """
        Return 종목의 가격이 3일간 하락했을 때, 2일 안에 추가 하락할 확률을 구한다.


        Parameters
        ----------
        df_close : DataFrame
            종목들의 close 값
        ticker : str
            종목


        Returns
        -------
        Prob : float
            하락할 확률
    """

    price = df_close

    Drop_1 = 0.05
    Drop_2 = 0.02
    window_1 = 3
    window_2 = 2
    first = []
    second = []
    Prob = 0

    df = pd.DataFrame(price[ticker])

    df['Date'] = price.index
    df['Drop_1'] = 1 * ((1 - Drop_1) * price[ticker].rolling(window_1).max() > price[ticker])
    df['Drop_2'] = 1 * ((1 - Drop_1 - Drop_2) * price[ticker].rolling(window_1 + window_2).max() > price[ticker])
    df.dropna(inplace=True)

    for i in range(len(df) - window_2):
        if df['Drop_1'].iloc[i] == 1:
            first.append(i)
            if df['Drop_2'].iloc[i + 1:i + window_2 + 1].sum() > 0:
                second.append(i)

    if len(first) == 0:  # 첫번째 급락이 발생하지 않은 경우
        Prob = 0
    else:
        Prob = len(second) / float(len(first))

    return Prob


def Kill_main(df_data, df_universe):

    """
        Return 하락 확률이 높은 종목에 대해, 급락을 감지하고 kill switch 적용 여부를 구한다.


        Parameters
        ----------
        df_data : DataFrame
            input data
        df_universe : DataFrame
            input universe


        Returns
        -------
        df_kill : DataFrame
            하락 확률이 높은 종목에 대해 kill switch 적용 여부
    """

    df_close = pd.merge(df_data.reset_index(), df_universe, left_on='Symbol', right_on='Ticker', how='inner')
    df_close = df_close.pivot('Date','Symbol','Close')

    kill_list=[]
    for t in df_close.columns.to_list():
        if prob_drop(df_close, t) >= 0.80: #and prob_drop(df_close,t)<=1.0:
            kill_list.append(t)
    df_kill=((df_close[kill_list]/(df_close[kill_list]).shift(3)) < 0.95).astype('int')
    df_kill_list = df_kill.reset_index().melt(id_vars='Date', var_name="Ticker", value_name='Score')
    df_kill_list = df_kill_list[df_kill_list['Score'] > 0]
    df_kill_list = pd.merge(df_kill_list, df_universe, left_on='Ticker', right_on='Ticker', how='inner').sort_values(['Date', 'Large','Mid'])
    df_kill_list = df_kill_list.rename(columns={'index': 'Date'}).set_index(['Date','Ticker'])
    return df_kill


def check_kill(df_universe, date, df_data, window):
    df_close = pd.merge(df_data.reset_index(), df_universe, left_on='Symbol', right_on='Ticker', how='inner')
    df_close = df_close.pivot('Date','Symbol','Close')
    num = df_close.index.get_loc(date)
    df_close = df_close.iloc[num - window:num + 1]

    kill_list=[]
    for t in df_close.columns.to_list():
        if prob_drop(df_close, t) >= 0.80: #and prob_drop(df_close,t)<=1.0:
            kill_list.append(t)
    df_kill=((df_close[kill_list]/(df_close[kill_list]).shift(3)) < 0.95).astype('int')
    df_kill = df_kill.loc[date]
    return df_kill


# date = date_list[-47]
# df_list = pd.DataFrame(['A130680'], columns=['Symbol'])
def check_symbol(df_list, date, window, df_data) :
    df_merge = pd.merge(df_data.reset_index(), df_list, left_on='Symbol', right_on='Symbol', how='inner')
    df_close = df_merge.pivot('Date', 'Symbol', 'Close')
    num = df_close.index.get_loc(date)
    df_close = df_close.iloc[num - window:num + 1]
    df_high = df_merge.pivot('Date', 'Symbol', 'High')
    df_high = df_high.iloc[num - window:num + 1]

    Drop_1 = 0.05
    Drop_2 = 0.02
    window_1 = 3
    window_2 = 2
    first = []
    second = []
    Prob = 0

    df = pd.DataFrame(df_close.index)
    df['Date'] = df_close.index
    df['Drop_1'] = 1 * ((1 - Drop_1) * df_high.rolling(window_1).max() > df_close)
    df['Drop_2'] = 1 * ((1 - Drop_1 - Drop_2) * df_high.rolling(window_1 + window_2).max() > df_close)
    df.dropna(inplace=True)

    for i in range(len(df) - window_2):
        if df['Drop_1'].iloc[i] == 1:
            first.append(i)
            if df['Drop_2'].iloc[i + 1:i + window_2 + 1].sum() > 0:
                second.append(i)

    if len(first) == 0:  # 첫번째 급락이 발생하지 않은 경우
        Prob = 0
    else:
        Prob = len(second) / float(len(first))

