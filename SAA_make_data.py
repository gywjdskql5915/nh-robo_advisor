import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from datetime import datetime
import warnings
warnings.filterwarnings('ignore')
from Wavelet import *

def concat_economic_data(economic_data,start_day,data,smoothed_data,universe):
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- concat_economic_data")
    economic_df = pd.DataFrame()
    economic_dict = {}

    for col in economic_data.columns:
        economic_dict[col] = economic_data[col].dropna(axis=0)

    for key in economic_dict.keys():

        df = pd.DataFrame(economic_dict[key])
        #smoothed_data에서 1년 전부터의 인덱스만 사용하기 위해
        df = df.loc[str(int(str(smoothed_data.index[0])[:4]) - 1):]
        scaler = MinMaxScaler(feature_range=(min(smoothed_data[data].loc[:start_day]),
                                                     max(smoothed_data[data].loc[:start_day])))
        scaler.fit(df.loc[:start_day])

        series = pd.Series(list(scaler.transform(df.values.reshape(1, -1)).reshape(1,-1)[0]),index = df.index, name = df.columns[0])
        series = series.pct_change(1).dropna(axis=0)


        economic_df[key] = pd.Series(series, index = smoothed_data.loc[series.index[0]:].index).ffill()
    economic_df = economic_df.dropna(axis=0)

    return economic_df


def make_HMM_data(df_economy_rate):
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- make_HMM_data")
    decay = 1
    window = 60
    feature_dict = {}
    self_decay = np.log(decay) / window
    decay = [np.exp(-i * self_decay) for i in range(0, window)]

    for data in df_economy_rate.columns:

        df = pd.DataFrame(columns=['change_{}'.format(i) for i in range(1, window + 1)], index=df_economy_rate.index)

        for i in range(window, len(df_economy_rate)):
            df.loc[df_economy_rate[data].iloc[i:i + 1].index[0]] = [
                np.log(df_economy_rate[data].iloc[i] / df_economy_rate[data].iloc[i - j]) for j in range(1, window + 1)]

        df = df.dropna(axis=0) * decay
        feature_dict[data] = df
    return feature_dict


def make_ML_data(smoothed_data, pred_period):
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- make_ML_data")
    '''
    make_ML_data(smoothed_data, rebelancing_period)
    기계학습에 사용하기 위한 데이터를 생성합니다. Feature와 label을 dictionary로 return하며, feature는 1일부터 60일까지의 log수익률로 이루어져 있으며, 최근일수록 높은 비중을 가집니다. Weight는 exponent로 주어집니다. Label은 pred_period에 맞춰 생성됩니다.

    -------
    Parameters
    smoothed_data : DataFrame
        스무딩된 SAA_data입니다.
    rebelancing_period : int
        Portfolio에서 사용되는 rebalancing 주기입니다. 해당 값에 의해 label에서 n일후의 등락여부가 결정됩니다.

    -------
    Returns
    feature_dict : dictionary
        Dictionary타입이며, key값이 asset의 이름으로 되어있고, value는 DataFrame입니다.
    Y_dict : dictionary
        Dictionary타입이며, key값이 asset의 이름으로 되어있고, value는 DataFrame입니다.

    '''

    decay = 1
    window = 60

    feature_dict = {}; Y_dict = {};
    self_decay  = np.log(decay) / (window)
    decay = [np.exp(-i*self_decay) for i in range(0,window)]

    for data in smoothed_data.columns:

        df = pd.DataFrame(columns = ['change_{}'.format(i) for i in range(1,window+1)], index = smoothed_data.index)

        for i in range(window,len(smoothed_data)):
            df.loc[smoothed_data[data].iloc[i:i+1].index[0]] = [np.log(smoothed_data[data].iloc[i]/smoothed_data[data].iloc[i-j]) for j in range(1,window+1)]

        df = df.dropna(axis=0) * decay
        feature_dict[data] = df
        Y_dict[data] = pd.DataFrame()

    return_ = smoothed_data.pct_change(pred_period).shift(-pred_period).dropna(axis=0)
    for data in smoothed_data.columns:
        Y_dict[data] = pd.concat([Y_dict[data],return_[data]],axis=1)


    for data in Y_dict.keys():
        Y_dict[data].columns =  ['Y_{}'.format(pred_period)]
        Y_dict[data] = Y_dict[data].dropna(axis=0)

        feature_dict[data].index = pd.DatetimeIndex(feature_dict[data].index)
        Y_dict[data].index = pd.DatetimeIndex(Y_dict[data].index)

        for indx in feature_dict[data].loc[Y_dict[data].index[-1]:].index:
            Y_dict[data].loc[indx] = Y_dict[data]['Y_{}'.format(pred_period)].loc[Y_dict[data].index[-1]]


    for data in Y_dict.keys():
        inter_index = Y_dict[data].index & feature_dict[data].index

        feature_dict[data] = feature_dict[data].loc[inter_index]
        Y_dict[data] = pd.DataFrame(np.where(Y_dict[data].loc[inter_index]>0,1,0),index = inter_index, columns = ['Y_{}'.format(pred_period)])

    return feature_dict, Y_dict


def make_economic_data(start_day,economic_data,feature_dict,smoothed_data,universe):
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- make_economic_data")
    '''
    make_economic_data(start_day,feature_dict,smoothed_data,universe)
    경제지표와 make_HMM_data, make_ML_data에 의해 생성된 데이터를 결합합니다.

    -------
    Parameters
    start_day : str
        concat_economic_data에서 사용하기 위해 포합됩니다.
    feature_dict : dictionary
        Make_{}_data에 의해 생성된 dictionary타입의 데이터 입니다.
    smoothed_data : DataFrame
        스무딩된 SAA_data입니다.
    universe : 사용하는 universe의 이름입니다. concat_economic_data에서 사용하기 위해 포합됩니다.

    -------
    Returns
    economic_dict : dictionary
        Dictionary타입이며, key값이 asset의 이름으로 되어있고, value는 DataFrame입니다.

    '''
    economic_dict = {}


    for data in feature_dict.keys():
        economic_df = concat_economic_data(economic_data,start_day,data,smoothed_data,universe)
        economic_dict[data] = pd.concat([feature_dict[data][['change_{}'.format(i) for i in range(10,61,2)]],
                   economic_df],axis=1).dropna(axis=0)

    return economic_dict


def SAA_make_data(SAA_method, start_day, raw_data, economic_data, universe, economic):
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- SAA_make_data")
    '''
    SAA_make_data(SAA_method,start_day,pred_period,raw_data,universe,economic)
    SAA_make_model.py에서 사용할 수 있도록 위의 함수들을 사용합니다.

    -------
    Parameters
    SAA_method : str
        Asset allocation에 사용할 모델명 입니다.
    start_day : str
        backtest에서 시작되는 날짜 입니다.
    pred_period : int
        Portfolio에서 사용되는 rebalancing 주기입니다.(191204-사용안함)
    raw_data : DataFrame
        asset에 대해서 선정된 대표index의 price
    universe : str
    사용하는 universe의 이름입니다. 경로를 설정하기 위해 사용됩니다.
    economic : Boolean
        경제지표 데이터 활용여부를 결정합니다.

    -------
    Returns
    -[HMM_dict,ML_dict,Y_dict] : list
        list형태로 0번째 값은 HMM에서 사용할 데이터로 dictionary타입 입니다. SAA_method가 Hybrid일 경우 3개의 returns이 필요하여, 불필요한 변수 설정을 줄이기 위해 list형태로 변환시켰습니다. 불필요한 데이터의 경우 0으로 설정하였습니다. (SAA_HMM으로 return을 받으면 list[0]에 값이 담겨있고, list[1] = 0, list[2] = 0 입니다.)


    '''
    pred_period=20
    smoothed_data = using_WT(raw_data)

    if SAA_method == 'HMM':
        HMM_dict = make_HMM_data(smoothed_data)
        ML_dict = 0
        Y_dict = 0
        if economic:
            HMM_dict = make_economic_data(start_day,economic_data,HMM_dict,smoothed_data,universe)
    elif SAA_method == 'ML':
        ML_dict, Y_dict = make_ML_data( smoothed_data, pred_period)
        HMM_dict = 0
        if economic:
            ML_dict = make_economic_data(start_day,economic_data,ML_dict,smoothed_data,universe)

    elif SAA_method == 'Hybrid':
        HMM_dict = make_HMM_data(smoothed_data)
        ML_dict, Y_dict = make_ML_data(smoothed_data, pred_period)
        if economic:
            HMM_dict = make_economic_data(start_day,economic_data,HMM_dict,smoothed_data,universe)
            ML_dict = make_economic_data(start_day,economic_data,ML_dict,smoothed_data,universe)

    return [HMM_dict,ML_dict,Y_dict]
