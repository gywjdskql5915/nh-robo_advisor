# -*- coding: utf-8 -*- 
# ----- imports ----- 
import numpy as np 
import pandas as pd 
import matplotlib.pyplot as plt 
import math 
from datetime import datetime, timedelta 
from dateutil.relativedelta import relativedelta 
import socket 
from collections import OrderedDict 
import glob 
import os 
from static_function import *
from scipy import signal

def calculate_mdd(df):
    window = len(df)
    top_n = 1
    drawdown = dict()
    max_date = dict()
    series = df + 1
    for date in df.index:
        drawdown[date] = (series.loc[date] - max(series.loc[:date])) \
                                                    / max(series.loc[:date])
        max_date[date] = series.loc[:date].idxmax()
    drawdown_df = pd.DataFrame()
    drawdown_df['Drawdown'] = pd.Series(drawdown).dropna()
    drawdown_df['Max_date'] = pd.Series(max_date).dropna()
    peak_index = signal.find_peaks(-drawdown_df['Drawdown'], distance= 100)[0]
    drawdown_df = drawdown_df.iloc[peak_index].nsmallest(n = top_n, columns='Drawdown').sort_index()
    return drawdown_df['Drawdown'][0]


if socket.gethostname() == 'probont01' or socket.gethostname() == 'drobont01': 
    kr_enc = 'utf-8' 
else: 
    kr_enc = 'CP949' 

pd.options.display.float_format = '{:.2f}'.format 
pd.set_option('display.max_columns', 20) 
pd.set_option('display.width', 1000)


def get_CAGR(accRet, distance_day):
    if isinstance(distance_day, str): 
        distance_day = timedelta(days=int(distance_day)) 
    if isinstance(distance_day, int): 
        distance_day = timedelta(days=distance_day) 
    year_period = distance_day.days / 365.0 
    cagr =((1 + accRet) ** (1 / year_period)) - 1 
    return cagr 


def get_mp_from_csv(mp_path, tendency_code, start_date="19000101", end_date="99999999"): 
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " get_mp_from_csv") 

    # 등록일자,자문사코드,상품코드,계좌번호,성향코드,자동생성여부,상품구분,국가코드,종목코드,비중,PF생성일자,등록사유, AP시그널 
    col_list = ['RegDate', 'CompanyCode', 'ProductCode', 'Account', 'TendencyCode', 'AutoMake', 'UniverseCode', 'CountryCode', 'Symbol', 
                      'Ratio', 'BaseDate', 'Reason','APMake'] 
    df_mp = pd.DataFrame(columns=col_list) 
    for file_name in glob.glob(mp_path + "/*.csv") : 
        if ((file_name[-12:-4:] >=  start_date) and (file_name[-12:-4:] <= end_date)) : 
            df = pd.read_csv(file_name, header=None, encoding=kr_enc, names = col_list) 
            df_mp = df_mp.append(df) 
    df_mp['Date'] = pd.to_datetime(df_mp['RegDate'], format='%Y%m%d') 
    df_mp['RegDate'] = pd.to_datetime(df_mp['RegDate'], format='%Y%m%d') 
    df_mp['BaseDate'] = pd.to_datetime(df_mp['BaseDate'], format='%Y%m%d') 
    df_mp['TendencyCode'] = df_mp['TendencyCode'].astype(str) 
    df_mp['TendencyCode'] = df_mp['TendencyCode'].apply(lambda x: x.zfill(2)) 
    df_mp['APMake'] = df_mp['APMake'].replace(np.nan , 'Y', regex=True) 
    df_mp = df_mp.loc[df_mp['TendencyCode'] == tendency_code] 
    return df_mp[['RegDate','Symbol','Ratio', 'BaseDate', 'Reason', 'APMake']] 


class BackTest: 
    def __init__(self, df_mp, df_data, data_list, capital, fee_rate, tax_rate, asset_ratio, company_code, product_code, tendency_code):
        print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " BackTest Init...")
        self.df_mp = df_mp[df_mp['TendencyCode'] == tendency_code]
        self.df_mp['RegDate'] = pd.to_datetime(self.df_mp['RegDate'], format="%Y%m%d")
        self.df_mp['BaseDate'] = pd.to_datetime(self.df_mp['BaseDate'], format="%Y%m%d")
        self.df_mp = pd.merge(self.df_mp, df_data.reset_index(), left_on=['BaseDate', 'Symbol'], right_on=['Date', 'Symbol'], how='inner')
        self.df_mp = self.df_mp[['RegDate', 'Symbol', 'Ratio', 'TendencyCode', 'BaseDate', 'Reason', 'APMake', 'Close']]

        self.capital = capital  # 최초 투입자금
        self.deposit = capital  # 예수금 
        self.total_asset = capital  # 순자산금액(예수금 + 계좌 잔고 평가금액) 

        self.date_list = data_list

        self.asset_ratio = asset_ratio 
        self.fee_rate = fee_rate 
        self.tax_rate = tax_rate

        # 잔고 테이블 : 종목코드, 잔고, 매입가, 현재가, 손익, 수익률, 평가금액, 매입일 
        self.balance_columns = ['Symbol', 'Balance', 'BuyPrice', 'NowPrice', 'Profit', 'Return', 'Evaluated', 'BuyDate', 'ProfitCut', 'LossCut'] 
        self.dfBalance = pd.DataFrame(columns=self.balance_columns).set_index('Symbol') 

        self.balance_list_columns = ['Date', 'Symbol', 'Balance', 'BuyPrice', 'NowPrice', 'Profit', 'Return', 'Evaluated', 'BuyDate', 'ProfitCut', 'LossCut'] 
        self.dfBalanceList = pd.DataFrame(columns=self.balance_list_columns) 

        # 계좌 테이블 : 날짜, 예수금, 순자산금액, 매입원가, 평가금액, 손익, 계좌 수익률 
        self.account_columns = ['Date', 'Deposit', 'TotalAsset', 'BuyPrice', 'EvalPrice', 'Profit', 'Return', 'AccReturn', 'Fee'] 
        self.dfAccount = pd.DataFrame(columns=self.account_columns).set_index('Date') 

        # 계좌 포트폴리오 테이블 
        self.account_port_columns = ['Date', 'Symbol', 'Order', 'Amount', 'Qty', 'AllocAmount', 'EvalAmount'] 
        self.df_actual_portfolio = pd.DataFrame(columns=self.account_port_columns).set_index(['Date', 'Symbol']) 

        # 속도 이슈로 data에서 MP에 있는 종목만 스크리닝 
        self.df_data = pd.merge(df_data.reset_index(), pd.DataFrame(df_mp['Symbol'].unique(), columns={'Symbol'}), left_on=['Symbol'], right_on=['Symbol'], how='inner')

        self.output_path = './' + company_code + "/" + product_code + '/Output/'
        if not os.path.exists(self.output_path): 
            os.makedirs(self.output_path) 

        # df_data.to_csv(self.output_path + '/raw_data.csv', sep=',', index=False, encoding=kr_enc)
        self.df_mp.sort_values(by=['RegDate', 'Symbol']).to_csv(self.output_path + '/mp.csv', sep=',', index=False, encoding=kr_enc)

    # 백테스트 실행 
    def run(self):
        print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " BackTest Run Start...") 
        for date in self.date_list:
            df_mp_day = self.df_mp[self.df_mp['RegDate'] == date] 
            day_fee = 0 
            day_tax = 0 
            if len(df_mp_day) > 0: 
                df_ap_day = self.make_actual_portfolio(date, df_mp_day, self.dfBalance) 
                self.df_actual_portfolio = pd.concat([self.df_actual_portfolio, df_ap_day]) 
                if len(df_ap_day) > 0 : 
                    day_fee, day_tax = self.order(date, df_ap_day, self.df_data)
            else :
                # Kill Switch 적용
                pass
            self.update_balance_evaluation(date, self.df_data)
            self.update_account_evaluation(date, day_fee, day_tax)
        print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " BackTest Run End...") 
        self.df_actual_portfolio = self.df_actual_portfolio.sort_values(by=['Date', 'Symbol'], ascending=True) 
        self.df_actual_portfolio[['Date', 'Symbol', 'Order', 'Amount', 'Qty', 'AllocAmount', 'EvalAmount']].to_csv( 
            self.output_path + 'actual_portfolio.csv', sep=',', index=False, encoding=kr_enc)
        self.dfBalanceList = self.dfBalanceList.sort_values(by=['Date', 'Symbol'], ascending=True) 
        self.dfBalanceList.to_csv(self.output_path + 'balance.csv', sep=',', index=False, encoding=kr_enc)
        self.dfAccount.to_csv(self.output_path + 'account.csv', sep=',', encoding=kr_enc)

    # 계좌 포트폴리오 생성 
    def make_actual_portfolio(self, date, df_mp_day, df_balance): 
        print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " BackTest make_actual_portfolio ") 
        df_ap_day = pd.DataFrame(columns=self.account_port_columns) 

        df_ap_day['Symbol'] = df_mp_day['Symbol'] 
        df_ap_day['BasePrice'] = df_mp_day['Close'] 
        df_ap_day['AllocAmount'] = (self.total_asset * self.asset_ratio * df_mp_day['Ratio'] * 0.01).astype(int) 

        # 잔고 정보 가져옴 
        # print ("<< MP >>") 
        # print (df_mp_day) 
        # print ("<< Balance >>") 
        # print (df_balance) 
        df_ap_day = pd.merge(df_ap_day, df_balance.reset_index(), left_on='Symbol', right_on='Symbol', how='outer') 
        df_ap_day['Date'] = date 
        df_ap_day['AllocAmount'].fillna(0, inplace=True) 
        df_ap_day['EvalAmount'] = df_ap_day['Evaluated'] 
        df_ap_day['EvalAmount'].fillna(0, inplace=True) 
        df_ap_day['Order'] = np.where(df_ap_day['AllocAmount'] >= df_ap_day['EvalAmount'], "B", "S") 
        df_ap_day['Amount'] = np.where(df_ap_day['Order'] == 'B', df_ap_day['AllocAmount'] - df_ap_day['EvalAmount'], 
                                       abs(df_ap_day['AllocAmount'] - df_ap_day['EvalAmount'])) 
        df_ap_day['Qty'] = np.where((df_ap_day['AllocAmount'] == 0) & (df_ap_day['Order'] == 'S'), df_ap_day['Balance'], 0) 

        df_ap_day = df_ap_day.drop(df_ap_day[(df_ap_day['Amount'] < df_ap_day['BasePrice']) & (df_ap_day['Order'] == 'B')].index) 
        df_ap_day = df_ap_day[['Date', 'Symbol', 'Order', 'Amount', 'Qty', 'AllocAmount', 'EvalAmount', 'BasePrice']] 
        df_ap_day = df_ap_day.sort_values(by=['Order'], ascending=False) 
        # print("<< AP >>") 
        # print(df_ap_day) 
        return df_ap_day 

    # 주문 실행 
    def order(self, date, df_ap_day, df_data) :
        print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " BackTest Order " + date.strftime("%Y-%m-%d")) 
        df_day = pd.merge(df_ap_day, df_data.reset_index(), left_on=['Date', 'Symbol'], right_on=['Date', 'Symbol'], how='inner').set_index(['Date','Symbol']) 
        df_day['OrderPrice'] = df_day['Open'] 
        # print("<< df_day >>") 
        # print(df_day) 

        day_fee = 0 
        day_tax = 0 
        for index in range(len(df_day)) : 
            price = float(df_day.iloc[index]['OrderPrice'])
            symbol = df_day.index.values[index][1] 
            if price == 0: 
                print ( "Warning! : " + symbol + " price is 0.") 
            elif (df_day.iloc[index]['Order'] == "S") : # 매도 시그널 
                qty = df_day.iloc[index]['Qty'] 
                if (qty == 0):  # 수량이 0이면 금액 매도 
                    qty = math.ceil(df_day.iloc[index]['Amount'] / price) # 소수점 이하 버림 
                    self.dfBalance.loc[(symbol), ('Balance')] -= qty 
                    print("Sell | 금액 {0} Price: {1} Qty: {2} Deposit: {3} ".format(symbol, str(price), str(qty), str(self.deposit))) 
                else:  # 수량이 0이 아니면 수량 매도 
                    self.dfBalance = self.dfBalance.drop([symbol]) 
                    print("Sell | 수량 {0} Price: {1} Qty: {2} Deposit: {3} ".format(symbol, str(price), str(qty), str(self.deposit))) 
                trading_price = int(price * qty) 
                fee_price = (int(trading_price * self.fee_rate) / 10) * 10  # 수수료는 10원단위 절사 
                # if (trading_price < 2000000) : 
                #     fee_price += 500 
                tax_price = int(trading_price * self.tax_rate) 
                self.deposit = self.deposit + (trading_price - fee_price - tax_price) 
                day_fee += fee_price 
                day_tax += tax_price 
                print( 
                    "Sell | 완료 {0} Total: {1} Fee: {2} Tax: {3} Deposit: {4} ".format(symbol, str(trading_price), str(fee_price), str(tax_price), 
                                                                                      str(self.deposit))) 
            elif (df_day.iloc[index]['Order'] == 'B'):  # 매수 시그널 
                if (price > self.deposit):  # 예수금이 기준가보다 클때만 매매가능 
                    print ("Buy  | 기준가 > 예수금 {0} Price: {1} Deposit: {2} ".format(symbol, str(price), str(self.deposit))) 
                elif (price > int(df_day.iloc[index]['Amount'])) : 
                    print ("Buy  | 기준가 > 종목대상금액 {0} Price: {1} Amount: {2} ".format(symbol, str(price), str(int(df_day.iloc[index]['Amount'])))) 
                else : 
                    qty = int(df_day.iloc[index]['Amount'] / price) 
                    while( price * qty > self.deposit) : 
                        print ("Buy  | 매매금액부족 {0} Price: {1} Qty: {2} Total: {3} Deposit: {4} ".format(symbol, str(price), str(qty), 
                                                                                                       str(price * qty), 
                                                                                                       str(self.deposit))) 
                        qty = qty - 1 
                    if (symbol in self.dfBalance.index):  # 잔고에 종목이 있는 경우(추가매수) 
                        self.dfBalance.loc[(symbol),('BuyPrice')] = (self.dfBalance.loc[symbol]['BuyPrice'] * self.dfBalance.loc[symbol][ 
                            'Balance'] + price * qty) / (self.dfBalance.loc[symbol]['Balance'] + qty) 
                        self.dfBalance.loc[(symbol),('Balance')] += qty 
                        self.dfBalance.loc[(symbol),('NowPrice')] = price 
                        self.dfBalance.loc[(symbol),('Evaluated')] = price * self.dfBalance.loc[symbol]['Balance'] 
                        self.dfBalance.loc[(symbol),('BuyDate')] = date 
                        print ("Buy  | 추가 {0} Price: {1} Qty: {2} Deposit: {3} ".format(symbol, str(price), str(qty), str(self.deposit))) 
                    else : 
                        order = pd.DataFrame({'Symbol': [symbol], 'Balance': qty, 'BuyPrice': price, 'NowPrice': price, 'Evaluated': price * qty, 
                                              'BuyDate': date}).set_index('Symbol') 
                        self.dfBalance = pd.concat([self.dfBalance, order]) 
                        print ("Buy  | 신규 {0} Price: {1} Qty: {2} Deposit: {3} ".format(symbol, str(price), str(qty), str(self.deposit))) 
                    trading_price = int(price * qty) 
                    fee_price = (int(trading_price * self.fee_rate) / 10) * 10 
                    # if (trading_price < 2000000): 
                    #     fee_price += 500 
                    self.deposit = self.deposit - trading_price - fee_price 
                    day_fee += fee_price 
                    print ("Buy  | 완료 {0} Total: {1} Fee: {2} Deposit: {3} ".format(symbol, str(trading_price), str(fee_price), str(self.deposit))) 
        return day_fee, day_tax 

    def update_balance_evaluation(self, date, df_data):
        self.dfBalance['Date'] = date
        self.dfBalance = pd.merge(self.dfBalance, df_data.reset_index(), left_on=['Date', 'Symbol'], right_on=['Date', 'Symbol'], how='inner').set_index(['Symbol'])
        self.dfBalance['NowPrice'] = self.dfBalance['Close']
        self.dfBalance['Evaluated'] = self.dfBalance['NowPrice'] * self.dfBalance['Balance'] 
        self.dfBalance['Profit'] = ((self.dfBalance['NowPrice'] - self.dfBalance['BuyPrice']) * self.dfBalance['Balance']).astype(int) 
        self.dfBalance['Return'] = (self.dfBalance['NowPrice'] / self.dfBalance['BuyPrice']) - 1
        self.dfBalance['Date'] = date
        self.dfBalance = self.dfBalance.drop(self.dfBalance[self.dfBalance['Balance'] == 0].index) 
        self.dfBalance = self.dfBalance[['Balance', 'BuyPrice', 'NowPrice', 'Profit', 'Return', 'Evaluated', 'BuyDate', 'ProfitCut', 'LossCut']]

        self.dfBalanceList = pd.concat([self.dfBalanceList, self.dfBalance.reset_index()]) 
        self.dfBalanceList = self.dfBalanceList[ 
            ['Date', 'Symbol', 'Balance', 'BuyPrice', 'NowPrice', 'Profit', 'Return', 'Evaluated', 'BuyDate', 'ProfitCut', 'LossCut']] 

    def update_account_evaluation(self, date, day_fee, day_tax): 
        totalAsset = float(self.deposit + self.dfBalance['Evaluated'].sum()) 
        buyPrice = (self.dfBalance['BuyPrice'] * self.dfBalance['Balance']).sum() 
        accReturn = float(totalAsset / self.capital) - 1 
        if (buyPrice == 0): 
            currReturn = 0 
        else: 
            currReturn = (self.dfBalance['Evaluated'].sum() / buyPrice) - 1 
        update = pd.DataFrame( 
            {'Date': date, 'Deposit': [self.deposit], 'TotalAsset': [totalAsset], 'BuyPrice': [buyPrice], 
             'EvalPrice': self.dfBalance['Evaluated'].sum(), 
             'Profit': self.dfBalance['Profit'].sum(), 'Return': currReturn, 'AccReturn': [accReturn], 'Fee': [day_fee], 'Tax': [day_tax] 
             }).set_index( 
            'Date') 
        self.TotalAsset = totalAsset 
        self.dfAccount = pd.concat([self.dfAccount, update]) 
        self.dfAccount = self.dfAccount[ 
            ['Deposit', 'TotalAsset', 'BuyPrice', 'EvalPrice', 'Profit', 'Fee', 'Return', 'AccReturn']] 
        print( 
            date.strftime("%Y-%m-%d") + " TotalAsset: " +  "{0:.2f}".format(
                self.dfAccount.loc[self.dfAccount.index == date]['TotalAsset'][0]) + " Return: " + "{0:.2f}".format(
                self.dfAccount.loc[self.dfAccount.index == date]['Return'][0] * 100) + " AccReturn: " + "{0:.2f}".format(
                self.dfAccount.loc[self.dfAccount.index == date]['AccReturn'][0] * 100))

    def draw_ret_chart(self, label):
        plt.rcParams["figure.figsize"] = (15, 8)
        cagr = get_CAGR(self.dfAccount.iloc[-1]['AccReturn'], self.dfAccount.index[-1] - self.dfAccount.index[0])
        p = plt.plot(self.dfAccount['AccReturn'],
                     label=label + " " + str(self.capital) + " " +
                           "AccRet:" + str(round(self.dfAccount['AccReturn'][-1] * 100, 2)) +
                           " CAGR:" + str(round(cagr * 100, 2)))
        rebal_date = pd.DataFrame({'Date': self.df_actual_portfolio['Date'].unique()})
        df_marker = pd.merge(self.dfAccount, rebal_date, left_index=True, right_on='Date', how='inner').set_index('Date')
        mdd = calculate_mdd(df_marker['AccReturn'])
        print('MDD Value: ',mdd)
        plt.plot(df_marker['AccReturn'], "o", color=p[-1].get_color(), label='')
        plt.legend(loc='upper left')
        plt.show()

    def draw_ret_chart_temp(self, label,name,universe):
        plt.clf()
        plt.rcParams["figure.figsize"] = (15, 8)
        cagr = get_CAGR(self.dfAccount.iloc[-1]['AccReturn'], self.dfAccount.index[-1] - self.dfAccount.index[0])
        p = plt.plot(self.dfAccount['AccReturn'],
                     label=label + " " + str(self.capital) + " " +
                           "AccRet:" + str(round(self.dfAccount['AccReturn'][-1] * 100, 2)) +
                           " CAGR:" + str(round(cagr * 100, 2)))
        rebal_date = pd.DataFrame({'Date': self.df_actual_portfolio['Date'].unique()})
        df_marker = pd.merge(self.dfAccount, rebal_date, left_index=True, right_on='Date', how='inner').set_index('Date')
        mdd = calculate_mdd(df_marker['AccReturn'])
        print('MDD Value: ',mdd)
        plt.plot(df_marker['AccReturn'], "o", color=p[-1].get_color(), label='')
        plt.legend(loc='upper left')
        plt.savefig(universe+'/'+name+'.png')
        #plt.show()
        return str(round(cagr * 100, 2)), mdd

    def draw_bm_chart(self, data, label):
        plt.plot(data)
        plt.legend(loc='upper left', label=label)
        plt.show()

