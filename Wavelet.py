import pandas as pd
import numpy as np
import pywt
import copy


def remove_points(number):

    """
        Return 1,000 단위 이상 수에서 ','를 제거해준다.


        Parameters
        ----------
        number : float
            임의의 수


        Returns
        -------
        number : float
            ','가 제거된 수
    """
    number=float(str(number).replace(',',''))
    return number


def get_wt_preprocessing(df):
    edata = list((WT(df[:100], lv=7, n=7,m=4))[0])
    for i in range(100, len(df)):
        edata.append(((WT(df[:i+1], lv=7, n=7,m=4))[0])[-1])
    return edata


def using_WT(raw_data):
    print(" --- using_WT")
    """
        Return 날 데이터를 받아 wavelet transform한 데이터를 출력한다.


        Parameters
        ----------
        raw_data : DataFrame
            날 데이터


        Returns
        -------
        wave_data : DataFrame
            wavelet transform된 데이터
    """

    filter_len = 8
    data_len = 1
    while True:
        if np.floor(np.log(data_len / (filter_len - 1)) / np.log(2)) == 4:
            break
        data_len += 1

    wave_data = pd.DataFrame()
    for col in raw_data.columns:
        WT_val = WT(raw_data[col].iloc[:data_len])[0][:-1]
        for i in range(data_len, len(raw_data[col]) + 1):
            WT_val = np.append(WT_val, WT(raw_data[col].iloc[:i])[0][-1])
        wave_data[col] = WT_val
    wave_data.index = raw_data.index
    wave_data = wave_data.dropna(axis=0)
    return wave_data


def WT(index_list, wavefunc='db4', lv=4, m=1, n=4, plot=False):

    """
        Return 열 데이터를 wavelet transform 하여 denoising한 계수를 반환해준다.


        Parameters
        ----------
        index_list : Series
            input 데이터
        wavefunc : str
            wavefunc='db4' : wavelet 함수
        lv: int
            lv=4 : denoising level
        m : int
             m=1 : threshold process level
        n : int
            n=4 : reconstruction level
        plot : bool
            plot=False


        Returns
        -------
        coeff : list
            denoising한 계수
    """

    # Decomposing
    coeff = pywt.wavedec(index_list, wavefunc, mode='sym',
                         level=lv)  # Decomposing by levels，cD is the details coefficient
    sgn = lambda x: 1 if x > 0 else -1 if x < 0 else 0  # sgn function

    # Denoising
    # Soft Threshold Processing Method
    for i in range(m,
                   n + 1):  # Select m~n Levels of the wavelet coefficients，and no need to dispose the cA coefficients(approximation coefficients)
        cD = coeff[i]
        Tr = np.sqrt(2 * np.log2(len(cD)))  # Compute Threshold
        for j in range(len(cD)):
            if cD[j] >= Tr:
                coeff[i][j] = sgn(cD[j]) * (np.abs(cD[j]) - Tr)  # Shrink to zero
            else:
                coeff[i][j] = 0  # Set to zero if smaller than threshold

    # Reconstructing
    coeffs = {}
    for i in range(len(coeff)):
        coeffs[i] = copy.deepcopy(coeff)
        for j in range(len(coeff)):
            if j != i:
                coeffs[i][j] = np.zeros_like(coeff[j])

    for i in range(len(coeff)):
        coeff[i] = pywt.waverec(coeffs[i], wavefunc)
        if len(coeff[i]) > len(index_list):
            coeff[i] = coeff[i][:-1]

    if plot:
        denoised_index = np.sum(coeff, axis=0)
        data = pd.DataFrame({'CLOSE': index_list, 'denoised': denoised_index})
        data.plot(figsize=(10, 10), subplots=(2, 1))
        data.plot(figsize=(10, 5))

    return coeff


def WT2(index_list, wavefunc='db4', lv=4, m=1, n=4, plot=False):

    """
        Return 열 데이터를 wavelet transform 하여 denoising한 계수를 반환해준다.


        Parameters
        ----------
        index_list : Series
            input 데이터
        wavefunc : str
            wavefunc='db4' : wavelet 함수
        lv: int
            lv=4 : denoising level
        m : int
             m=1 : threshold process level
        n : int
            n=4 : reconstruction level
        plot : bool
            plot=False


        Returns
        -------
        coeff : Series
            denoising한 계수
    """

    # Decomposing
    coeff = pywt.wavedec(index_list, wavefunc, mode='sym',
                         level=lv)  # Decomposing by levels，cD is the details coefficient
    sgn = lambda x: 1 if x > 0 else -1 if x < 0 else 0  # sgn function

    # Denoising
    # Soft Threshold Processing Method
    for i in range(m,
                   n + 1):  # Select m~n Levels of the wavelet coefficients，and no need to dispose the cA coefficients(approximation coefficients)
        cD = coeff[i]
        Tr = np.sqrt(2 * np.log2(len(cD)))  # Compute Threshold
        for j in range(len(cD)):
            if cD[j] >= Tr:
                coeff[i][j] = sgn(cD[j]) * (np.abs(cD[j]) - Tr)  # Shrink to zero
            else:
                coeff[i][j] = 0  # Set to zero if smaller than threshold

    # Reconstructing
    coeffs = {}
    for i in range(len(coeff)):
        coeffs[i] = copy.deepcopy(coeff)
        for j in range(len(coeff)):
            if j != i:
                coeffs[i][j] = np.zeros_like(coeff[j])

    for i in range(len(coeff)):
        coeff[i] = pywt.waverec(coeffs[i], wavefunc)
        if len(coeff[i]) > len(index_list):
            coeff[i] = coeff[i][:-1]

    return pd.Series(coeff[0], index=index_list.index)

