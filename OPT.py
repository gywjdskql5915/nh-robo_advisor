import numpy as np
import pandas as pd
#from Wavelet import *
import scipy.cluster.hierarchy as sch
from scipy.optimize import minimize
from static_function import *

def rb_p_weights(asset_rets, rb):
    """
    Portfolio의 weight를 Risk budget method를 사용하여 계산.
    ----------
    Parameters

    Asset_rets : DataFrame
    수익률의 DataFrame
    Rb : list
    자산의 Risk budgeting  (ex) Dual momentum score, 변동성, 1/자산의 수
    ----------
    Returns
    Weight: array
    각 자산들의 배분비율

    """

    def obj_fun(x, p_cov, rb):
        # risk budgeting approach optimisation object function
        return np.sum((x * np.dot(p_cov, x) / np.dot(x.transpose(), np.dot(p_cov, x)) - rb) ** 2)

    def cons_sum_weight(x):
        # constraint on sum of weights equal to one
        return np.sum(x) - 1.0

    def cons_long_only_weight(x):
        # constraint on weight larger than zero

        return x

    num_arp = asset_rets.shape[1]  # number of ARP series
    p_cov = asset_rets.cov()  # covariance matrix of asset returns
    w0 = 1.0 * np.ones((num_arp, 1)) / num_arp  # initial weights
    cons = ({'type': 'eq', 'fun': cons_sum_weight}, {'type': 'ineq', 'fun': cons_long_only_weight})  # constraints

    return minimize(obj_fun, w0, args=(p_cov, rb), method='SLSQP', constraints=cons).x  # portfolio optimisation


def HRP_bottom_up(ret):
    """
    Portfolio의 weight를 Hierarchical Risk Parity방법을 사용하여 계산. 계산 방식은 bottom up 방식이다.
    ----------
    Parameters
    ret : DataFrame
        자산 수익률의 DataFrame
    ----------
    Returns
    Weight: Series
        각 자산들의 배분비율

    """

    cov, corr = ret.cov(), ret.corr()
    link = sch.linkage(corr)
    link1 = []
    link2 = {}
    w = pd.Series(1., index=cov.index)
    # We get another link matrix that has integer values
    for j in range(len(link)):
        link1.append(link[j][:2].astype(int).tolist())
    # From above integer valued link matrix, we investigate the hierarchical structure.
    for k in range(len(link1)):
        [i, j] = link1[k]
        if i <= len(link1) and j <= len(link1):
            link2[k + 1] = [[i], [j]]
        elif i > len(link1) and j <= len(link1):
            link2[k + 1] = [[], [j]]
            for s in link2[i - len(link1)]:
                link2[k + 1][0] = link2[k + 1][0] + s
        elif i <= len(link1) and j > len(link1):
            link2[k + 1] = [[i], []]
            for l in link2[j - len(link1)]:
                link2[k + 1][1] = link2[k + 1][1] + l
        else:
            link2[k + 1] = [[], []]
            for s in link2[i - len(link1)]:
                link2[k + 1][0] = link2[k + 1][0] + s
            for l in link2[j - len(link1)]:
                link2[k + 1][1] = link2[k + 1][1] + l
    for s in range(1, len(cov)):
        [i, j] = link2[s]
        # weight calculation
        var_1 = np.dot(np.dot(w[i], cov.iloc[i, i]), w[i].T)
        var_2 = np.dot(np.dot(w[j], cov.iloc[j, j]), w[j].T)
        alpha = var_2 / (var_1 + var_2)
        w[i] = w[i] * alpha
        w[j] = w[j] * (1 - alpha)
    return w.fillna(0)


def OPT_NH(df_saa_ratio, df_taa_list, df_data, opt_method, start_date, end_date, df_universe, baseMPday_list=[], opt_window=60):
    df_saa_ratio = df_saa_ratio.reindex(pd.date_range(start=start_date, end=end_date, freq='D'), method='ffill')
    df_saa_ratio = df_saa_ratio.bfill()
    del df_saa_ratio['State_Name']

    eps = 1e-6
    df_close = pd.merge(df_data.reset_index(), df_universe, left_on='Symbol', right_on='Ticker', how='inner')
    df_close = df_close.pivot('Date','Symbol','Close')

    dates, date_list = get_operation_date(df_data, start_date, end_date)
    portfolio_weight = pd.DataFrame(index=dates, columns=df_close.columns)

    large_tickers = list(set(df_universe.Large))
    if len(baseMPday_list) > 0:
        dates = baseMPday_list
    for date in dates:
        df_saa_day = df_saa_ratio.loc[date]
        print(df_taa_list)
        df_taa_day = df_taa_list.loc[date]
        day_stock_list = list(df_taa_day.index)
        df_daily_return = (df_close[day_stock_list].pct_change().loc[:date].iloc[-opt_window:]).dropna()
        for large in large_tickers:
            day_large_list = list(df_taa_day[df_taa_day['Large'] == large].index)
            if len(day_large_list) == 1:
                portfolio_weight.loc[date][day_large_list] = 1 * df_saa_day[large]
            elif len(day_large_list) > 1:
                if opt_method == 'EW':
                    weight = pd.Series(1./len(day_large_list),index=day_large_list)
                    portfolio_weight.loc[date][day_large_list] = (weight * (weight > eps)) * df_saa_day[large]
                elif opt_method == 'RP':
                    risk_budget = [1 / len(day_large_list)] * len(day_large_list)  # np.std(daily_return)
                    weight = rb_p_weights(df_daily_return[day_large_list], risk_budget)
                    portfolio_weight.loc[date][day_large_list] = (weight * (weight > eps)) * df_saa_day[large]
                elif opt_method == 'HRP':
                    weight = HRP_bottom_up(df_daily_return[day_large_list])
                    portfolio_weight.loc[date][day_large_list] = (weight * (weight > eps)) * df_saa_day[large]
                elif opt_method == 'RB':
                    risk_budget = df_taa_day.loc[day_large_list]['Score']  # np.std(daily_return)
                    risk_budget /= risk_budget.sum()
                    weight = rb_p_weights(df_daily_return[day_large_list], risk_budget)
                    portfolio_weight.loc[date][day_large_list] = (weight * (weight > eps)) * df_saa_day[large]
    portfolio_weight = portfolio_weight.fillna(0)
    return portfolio_weight


def OPT_KO(df_saa_ratio,df_selected_score, df_data, opt_method, start_date, end_date, df_universe, universe, df_killed, opt_window=60):

    # initialization

    eps = 1e-6
    df_close = df_divide(df_data, 'Close',df_universe)

    portfolio_weight = pd.DataFrame(index=df_close.index, columns=df_universe.Ticker)
    rebal_index = make_interval_date(get_operation_date(df_data, start_date, end_date), start_date, end_date,
                                     month_interval=1)
    datetime.strptime(start_date, '%Y-%m-%d')
    ###########################################################################
    large_tickers = list(set(df_universe.Large))
    new_index = pd.date_range(start=start_date, end=df_close.index[-1], freq='D')
    new_df_saa_ratio = df_saa_ratio.reindex(new_index, method='ffill')
    new_df_saa_ratio = new_df_saa_ratio.bfill()
    ############################################################################
    dates=get_operation_date(df_data, start_date, end_date)

    # for loop
    for num, date in enumerate(dates):
        if num == 0:
            portfolio_weight.loc[date] = 0
        # rebalancing_day
        else:
            if date in rebal_index:
                temp = df_selected_score.loc[date]
                selected_tickers = list((temp[temp > 0].keys()))
                ###############################################################################
                large_weight = new_df_saa_ratio[large_tickers].loc[date]

                non_zero_large_cat = 0
                portfolio_weight_copy = pd.Series(portfolio_weight.loc[date], index=df_universe.Ticker)
                #############################################################################
                df_close_bussiness = df_close.loc[dates]
                daily_return = (df_close_bussiness[selected_tickers].pct_change().loc[:date].iloc[-opt_window:]).dropna()

                if len(selected_tickers) == 0:
                    weight_series = pd.Series(index=df_universe.Ticker)
                    portfolio_weight.loc[date] = weight_series.fillna(0).values

                elif len(selected_tickers) == 1:
                    weight_series = pd.Series(index=df_universe.Ticker)
                    weight_series[selected_tickers] = 1
                    portfolio_weight.loc[date] = weight_series.fillna(0).values

                elif opt_method == 'RB':
                    risk_budget = df_selected_score[selected_tickers].loc[date]  # np.std(daily_return)
                    risk_budget /= risk_budget.sum()  # 2019-10-10
                    weight = rb_p_weights(daily_return, risk_budget)
                    weight_series = pd.Series(index=df_universe.Ticker)
                    weight_series[selected_tickers] = weight * (weight > eps)

                    portfolio_weight.loc[date] = weight_series.fillna(0).values

                elif opt_method == 'HRP':

                    weight = HRP_bottom_up(daily_return)
                    weight_series = pd.Series(index=df_universe.Ticker)
                    weight_series[selected_tickers] = weight * (weight > eps)

                    portfolio_weight.loc[date] = weight_series.fillna(0).values

                elif opt_method == 'RP':
                    risk_budget = [1/len(selected_tickers)]*len(selected_tickers)  # np.std(daily_return)
                    weight = rb_p_weights(daily_return, risk_budget)
                    weight_series = pd.Series(index=df_universe.Ticker)
                    weight_series[selected_tickers] = weight * (weight > eps)

                elif opt_method == 'EW':
                    weight_series = pd.Series(index=df_universe.Ticker)
                    weight_series[selected_tickers] = 1.0 / len(selected_tickers)
                    portfolio_weight.loc[date] = weight_series.fillna(0).values
                else:
                    print("Select Asset Optimization Method: RB, Equal")
                ###############################################################################################
                for large_cat in large_tickers:
                    if portfolio_weight_copy[(df_universe[df_universe.Large == large_cat]).Ticker].sum() > eps:
                        non_zero_large_cat += large_weight[large_cat]
                        portfolio_weight_copy[(df_universe[df_universe.Large == large_cat]).Ticker] *= large_weight[large_cat] / portfolio_weight_copy[(df_universe[df_universe.Large == large_cat]).Ticker].sum()

                ###############################################################################################
            else:
                pass
                for c in df_killed.columns:
                    if df_killed[c].loc[last_day] == 1:
                        df_killed[c].loc[date] = 1

                # prev_date = df_close.index[len((df_close.loc[:date]).index) - 2]

                portfolio_weight.loc[date] = portfolio_weight.loc[last_day]
        last_day = date
    portfolio_weight.loc[start_date:].to_csv(universe + '/Intermediate/df_weight.csv')
    return portfolio_weight.loc[start_date:], rebal_index
