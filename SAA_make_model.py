from SAA_make_data import *
import hmmlearn.hmm as hmm
from sklearn.ensemble import RandomForestClassifier
import sklearn.metrics as met
from datetime import datetime
from Wavelet import *

def make_HMM(smoothed_data , feature_dict, start_day, rebalancing_period, convert):
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- make_HMM")
    '''
    make_HMM(smoothed_data , feature_dict, start_day, rebelancing_period, convert)
    HMM을 이용하여 국면을 결정합니다. rebalancing_period마다 추가된 데이터를 반영하는 모델을 재생성하며, HMM의 결과는 0,1만을 출력하지만 해당 값에 등락의 개념이 없기 때문에 start_day 이전의 데이터를 이용하여 등락을 결정합니다. Asset에 대해 각각 구한 value를 이용하여 최종 국면의 state를 반환합니다.

    -------
    Parameters

    smoothed_data : DataFrame
        스무딩된 SAA_data입니다. 결과값에 대한 등락을 판단하고, index를 부여하기 위해 사용합니다.
    feature_dict : dictionary
        Make_HMM_data에 의해 생성된 dictionary타입의 데이터 입니다.
    start_day : str
        backtest에서 시작되는 날짜 입니다.
    rebelancing_period : int
        Portfolio에서 사용되는 rebalancing 주기입니다.
    convert : Boolean
        물가U = 채권D로 가정하기 때문에 asset에서 Bond를 사용할 경우, True로 설정합니다.

    -------
    Returns

    state : DataFrame
        HMM에 의해 결정된 국면에 대한 state입니다. Rebelacning_period와 상관없이 daily로 값이 채워져 있으며, [-2, -1, 1, 2]로 이루어져 있습니다. column은 HMM으로 되어있습니다.

    '''
    def model(num,asset,feature,day):

        train = feature.loc[:day]
        test = feature.loc[day:]

        model = hmm.GaussianHMM(n_components=num,n_iter=100,random_state = 1)
        model.fit(train)

        state_train = pd.Series(model.predict(train), index = train.index, name = asset)
        state_test = pd.Series(model.predict(test), index = test.index, name = asset)
        return state_train,state_test

    def state_class(train,test,asset,smoothed_data):

        return1 = smoothed_data[asset].pct_change(5).dropna(axis=0)
        return2 = smoothed_data[asset].pct_change(20).dropna(axis=0)
        return3 = smoothed_data[asset].pct_change(40).dropna(axis=0)
        return_ = pd.concat([return1,return2,return3],axis=1).mean(axis=1).dropna(axis=0)
        df = train.replace(0,'down').replace(1,'up')
        return_val = pd.Series(np.where(return_.loc[df.index] > 0,1,0), index = return_.loc[df.index].index)

        val_1 = sum(return_val.loc[df[df == 'up'].index])/len(return_val.loc[df[df == 'up'].index])
        val_2 = sum(return_val.loc[df[df == 'down'].index])/len(return_val.loc[df[df == 'down'].index])

        if val_1 >= val_2:
            class_value = {0:'down', 1:'up'}
        else:
            class_value = {0:'up', 1:'down'}

        return class_value

    def convert_value(data):
        data = data.replace('down',0).replace('up',1)
        # 물가U = 채권D을 반영하기 위함
        data = data.replace(0,'up').replace(1,'down')
        return data


    def make_state(state_1, state_2):

        state_df = pd.Series(index = state_1.index, name = 'state')
        state_df.loc[state_1[state_1 == 'up'][state_2 == 'up'].index] = 2
        state_df.loc[state_1[state_1 == 'up'][state_2 == 'down'].index] = 1
        state_df.loc[state_1[state_1 == 'down'][state_2 == 'down'].index] = -1
        state_df.loc[state_1[state_1 == 'down'][state_2 == 'up'].index] = -2

        return state_df


    state_series = pd.Series()
    data_1 = smoothed_data.columns[0]
    data_2 = smoothed_data.columns[1]
    train_1, test_1 = model(2,data_1,feature_dict[data_1],smoothed_data.loc[start_day:].iloc[::rebalancing_period].index[0])
    train_2, test_2 = model(2,data_2,feature_dict[data_2],smoothed_data.loc[start_day:].iloc[::rebalancing_period].index[0])
    class_value1 = state_class(train_1,test_1,data_1,smoothed_data)
    class_value2 = state_class(train_2,test_2,data_2,smoothed_data)



    for day in smoothed_data.loc[start_day:].iloc[::rebalancing_period].index:

        train_1, test_1 = model(2,data_1,feature_dict[data_1],day)
        train_2, test_2 = model(2,data_2,feature_dict[data_2],day)
        state_1 = test_1.replace(class_value1)
        state_2 =test_2.replace(class_value2)
        if convert:
            state_2 = convert_value(state_2)
        state_ = make_state(state_1, state_2)
        state_series.loc[day] = state_.iloc[:rebalancing_period].mode().values[0]


    state = pd.DataFrame(state_series, index = smoothed_data.loc[start_day:].index, columns = ['HMM']).fillna('n')
    state.index.name = 'Date'
    for i in range(len(state)):
        if state[state.columns[0]].iloc[i] == 'n':
            state[state.columns[0]].iloc[i] = state[state.columns[0]].iloc[i-1]

    return state


def make_ML(smoothed_data, feature_dict, Y_dict, start_day, rebalancing_period,convert):
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- make_ML")
    '''
    make_ML(smoothed_data, feature_dict, Y_dict, start_day, rebelancing_period, convert)
    기계학습을 이용하여 국면을 결정합니다. daily마다 추가된 데이터를 반영하는 모델을 재생성하며, 모든 예측은 rebalancing_period후를 예측합니다. rebalancing_period기간동안 예측한 value에다 exponent weight를 준 후, voting하여 최종 pred를 산출합니다. Precision condition에 의해 성능이 떨어지는 구간에서의 value를 개선시키며, Asset에 대해 각각 구한 value를 이용하여 최종 국면의 state를 반환합니다.

    -------
    Parameters

    smoothed_data : DataFrame
        스무딩된 SAA_data입니다. 결과값에 대한 등락을 판단하고, index를 부여하기 위해 사용합니다.
    feature_dict : dictionary
        Make_ML_data에 의해 생성된 dictionary타입의 데이터 입니다.
    Y_dict : dictionary
        Make_ML_data에 의해 생성된 dictionary타입의 데이터 입니다.
    start_day : str
        backtest에서 시작되는 날짜 입니다.
    rebelancing_period : int
        Portfolio에서 사용되는 rebalancing 주기입니다.
    convert : Boolean
        물가U = 채권D로 가정하기 때문에 asset에서 Bond를 사용할 경우, True로 설정합니다.

    -------
    Returns

    state : DataFrame
        ML에 의해 결정된 국면에 대한 state입니다. Rebelacning_period와 상관없이 daily로 값이 채워져 있으며, [-2, -1, 1, 2]로 이루어져 있습니다. column은 ML으로 되어있습니다.

    '''
    precision_window = 90
    pred_period = 20
    indicator=False
    tol=0.4
    voting=False

    def modeing(feature_dict, Y_dict, start_day,pred_period=pred_period):

        model_df = pd.DataFrame(index = feature_dict[list(feature_dict.keys())[0]].index,
                            columns = ['{}_true'.format(list(feature_dict.keys())[0]),
                                       '{}_pred'.format(list(feature_dict.keys())[0]),
                                       '{}_true'.format(list(feature_dict.keys())[1]),
                                       '{}_pred'.format(list(feature_dict.keys())[1])])
        test_day = feature_dict[list(feature_dict.keys())[0]].iloc[len(feature_dict[list(feature_dict.keys())[0]].loc[:start_day])-pred_period:].index[0]
        test_day2 = feature_dict[list(feature_dict.keys())[0]].iloc[len(feature_dict[list(feature_dict.keys())[0]].loc[:start_day])-pred_period*2:].index[0]

        for data in feature_dict.keys():
            model = RandomForestClassifier(n_jobs=-1,random_state = 1,n_estimators = 60,
            criterion = 'entropy',warm_start=True)
            model.fit(feature_dict[data].loc[:test_day2],Y_dict[data].loc[:test_day2])

            pred_val = np.array([])
            pred_val = np.append(pred_val,model.predict(feature_dict[data].loc[:test_day]))

            for indx in feature_dict[data].iloc[len(feature_dict[data].loc[:test_day])-pred_period:-pred_period].index:
                pred_val = np.append(pred_val,model.predict(feature_dict[data].loc[indx:].iloc[pred_period:pred_period+1]))
                model.fit(feature_dict[data].loc[:indx],Y_dict[data].loc[:indx])

            model_df['{}_true'.format(data)]= Y_dict[data].values
            model_df['{}_pred'.format(data)] = pred_val

        return model_df.astype(int)


    def indicator_function(true_df, pred_df,tol, pred_period, window = precision_window):
        tol = round(tol,2)
        df = pd.Series(name = 'tol_{}'.format(tol))

        for i in range((window+pred_period),len(true_df)):
            val = met.precision_score(true_df.iloc[(i-window-pred_period):(i-pred_period)],
                                      pred_df.iloc[(i-window-pred_period):(i-pred_period)],labels=[1,0])
            if val >= tol:
                df.loc[pred_df.iloc[i:i+1].index[0]] = pred_df.iloc[i]
            elif val < tol:
                df.loc[pred_df.iloc[i:i+1].index[0]] = 0

        df = df.astype(int)
        pred_df.loc[df.index] = df.values

        df1 = pd.Series(name = 'tol_{}'.format(tol))

        for i in range((window+pred_period),len(true_df)):
            val = met.confusion_matrix(true_df.iloc[(i-window-pred_period):(i-pred_period)],
                                      pred_df.iloc[(i-window-pred_period):(i-pred_period)],labels=[1,0])
            if val[1,1] == 0:
                val1 = 0
            else:
                val1 = val[1,1]/(val[0,1] + val[1,1])

            if val1 >= tol:
                df1.loc[df.index[i-(window+pred_period)]] = df.values[i-(window+pred_period)]
            elif val1 < tol:
                df1.loc[df.index[i-(window+pred_period)]] = 1

        return df1


    def voting_function(df,rebalancing_period):

        voting_df = pd.DataFrame(columns = df.columns)

        for i in range(0,len(df)-1,rebalancing_period):
            voting_count=len(df.iloc[i:i+rebalancing_period])
            decay = 5
            self_decay  = np.log(decay) / (voting_count)
            decay = np.array([np.exp(-i*self_decay) for i in range(voting_count,0,-1)])
            val = df.iloc[i:i+voting_count] * np.repeat(decay,4).reshape(-1,4)
            voting_df.loc[df.iloc[i:i+voting_count].index[-1]] = np.where(val.sum(axis=0) > decay.sum(axis=0)/2,1,0)

        return voting_df

    def make_state(start_day,smoothed_data, data_1,data_2):

        def convert_value(data):
            data = data.replace(0,'down').replace(1,'up')
            # 물가U = 채권D을 반영하기 위함
            data = data.replace('down',1).replace('up',0)
            return data

        def set_phase(data_1,data_2):
            phase_df = pd.Series(index = data_1.index)
            phase_df.loc[data_1[data_1 == 1][data_2 == 1].index] = 2
            phase_df.loc[data_1[data_1 == 1][data_2 == 0].index] = 1
            phase_df.loc[data_1[data_1 == 0][data_2 == 0].index] = -1
            phase_df.loc[data_1[data_1 == 0][data_2 == 1].index] = -2

            return phase_df

        if convert:
            data_2 = convert_value(data_2)
        state_ = set_phase(data_1,data_2)
        state_series = pd.Series(state_, index = smoothed_data.loc[start_day:].index, name = 'ML').fillna('n')
        for i in range(len(state_series)):
            if state_series.iloc[i] == 'n':
                state_series.iloc[i] = state_series.iloc[i-1]

        return state_series



    model_df = modeing(feature_dict, Y_dict, start_day)

    if indicator:
        ML_df=pd.DataFrame()
        ML_df[model_df.columns[0]] = model_df[model_df.columns[0]]
        ML_df[model_df.columns[1]] = indicator_function(model_df[model_df.columns[0]],model_df[model_df.columns[1]],tol, pred_period)
        ML_df[model_df.columns[2]] = model_df[model_df.columns[2]]
        ML_df[model_df.columns[3]] = indicator_function(model_df[model_df.columns[2]],model_df[model_df.columns[3]],tol, pred_period)
        ML_df = ML_df.dropna(axis=0).astype(np.float64)
    else:
        ML_df=model_df.copy()

    if voting:
        voting_df=voting_function(ML_df,pred_period)
        state=pd.DataFrame(make_state(start_day,smoothed_data, voting_df.iloc[:,1], voting_df.iloc[:,3]))
    else:
        state=pd.DataFrame(make_state(start_day,smoothed_data, ML_df.iloc[:,1], ML_df.iloc[:,3]))


    return state


def make_hybrid(HMM_state,ML_state):
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- make_hybrid")
    '''
    make_hybrid(HMM_state,ML_state)
    HMM과 ML에 의한 state의 논리곱을 이용하여 국면을 새롭게 정합니다.

    -------
    Parameters

    HMM_state : DataFrame
        make_HMM에 의해 return된 state입니다.

    ML_state : DataFrame
        make_ML에 의해 return된 state입니다.

    -------
    returns

    pd.DataFrame(hybrid) : DataFrame
        Hybrid에 의해 결정된 국면에 대한 state입니다. Rebelacning_period와 상관없이 daily로 값이 채워져 있으며, [-2, -1, 1, 2]로 이루어져 있습니다. column은 Hybrid로 되어있습니다.

    '''

    #aggressive
    hybrid = pd.Series(index = HMM_state.index, name = 'Hybrid')

    hybrid.loc[HMM_state[HMM_state == 2][ML_state == 2].index] = 2
    hybrid.loc[HMM_state[HMM_state == 1][ML_state == 2].index] = -2
    hybrid.loc[HMM_state[HMM_state == -1][ML_state == 2].index] = 2
    hybrid.loc[HMM_state[HMM_state == -2][ML_state == 2].index] = -2

    hybrid.loc[HMM_state[HMM_state == 2][ML_state == 1].index] = 1
    hybrid.loc[HMM_state[HMM_state == 1][ML_state == 1].index] = -1
    hybrid.loc[HMM_state[HMM_state == -1][ML_state == 1].index] = 1
    hybrid.loc[HMM_state[HMM_state == -2][ML_state == 1].index] = -1

    hybrid.loc[HMM_state[HMM_state == 2][ML_state == -1].index] = 2
    hybrid.loc[HMM_state[HMM_state == 1][ML_state == -1].index] = -2
    hybrid.loc[HMM_state[HMM_state == -1][ML_state == -1].index] = 2
    hybrid.loc[HMM_state[HMM_state == -2][ML_state == -1].index] = -2

    hybrid.loc[HMM_state[HMM_state == 2][ML_state == -2].index] = 1
    hybrid.loc[HMM_state[HMM_state == 1][ML_state == -2].index] = -1
    hybrid.loc[HMM_state[HMM_state == -1][ML_state == -2].index] = 1
    hybrid.loc[HMM_state[HMM_state == -2][ML_state == -2].index] = -1


    hybrid.index.name = 'Date'

    return pd.DataFrame(hybrid)


def SAA_make_model(SAA_method,start_day,rebalancing_period,raw_data,economic_data,universe,economic,convert):
    print(datetime.now().time().strftime("%H:%M:%S:%f")[:-4] + " --- SAA_make_model")
    '''
    SSA_make_model(SAA_method,start_day,rebalancing_period,raw_data,universe,economic,convert)
    SAA_make_model.py에서 사용하는 함수를 제어합니다.

    -------
    Parameters

    SAA_method : str
        Asset allocation에 사용할 모델명 입니다.
    start_day : str
        backtest에서 시작되는 날짜 입니다.
    rebalancing_period : int
        Portfolio에서 사용되는 rebalancing 주기입니다.
    raw_data : DataFrame
        asset에 대해서 선정된 대표index의 price
    universe : str
    사용하는 universe의 이름입니다. 경로를 설정하기 위해 사용됩니다.
    economic : Boolean
        경제지표 데이터 활용여부를 결정합니다.
    convert : Boolean
        convert사용여부를 결정합니다.

    -------
    Returns

    result : DataFrame
        SAA_make_model.py를 활용하여 생성된 국면을 반환합니다.

    '''
    smoothed_data = using_WT(raw_data)
    if SAA_method == 'HMM':
        data = SAA_make_data(SAA_method,start_day,raw_data,economic_data,universe,economic)
        HMM_dict = data[0]
        result = make_HMM(smoothed_data , HMM_dict, start_day, rebalancing_period,convert)

    elif SAA_method == 'ML':
        data = SAA_make_data(SAA_method,start_day,raw_data,economic_data,universe,economic)
        ML_dict,Y_dict = data[1],data[2]
        result = make_ML(smoothed_data, ML_dict, Y_dict, start_day, rebalancing_period,convert)

    elif SAA_method == 'Hybrid':
        data = SAA_make_data(SAA_method,start_day,raw_data,economic_data,universe,economic)
        HMM_dict,ML_dict,Y_dict = data[0],data[1],data[2]
        HMM = make_HMM(smoothed_data , HMM_dict, start_day, rebalancing_period,convert)
        ML = make_ML(smoothed_data, ML_dict, Y_dict, start_day, rebalancing_period,convert)
        result = make_hybrid(HMM['HMM'],ML['ML'])
    return result
